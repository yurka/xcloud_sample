
import asyncio

import logging.config
from union.bus.aio import RmqServiceBus
from union.service import Service, register, subscribe


logging.config.fileConfig('logging.conf')


class ThreeService(Service):

    async def start(self, ioloop):
        await super().start(ioloop)
        self.log.info('UP!')
    #     asyncio.ensure_future(self.job())

    # async def job(self):
    #     while True:
    #         word = _words.__next__()
    #         res = await self.bus.rpc_call('union_two.process_word', word)
    #         self.log.info('Word: %s, Result: %s', word, res)

    @register()
    async def to_upper(self, word):
        """
        Process given word
        """
        res = await self.bus.rpc_call('union_one.reverse', word)
        return res.upper()

    @subscribe('broadcast')
    def listen_broadcast(self, message):
        self.log.info('Broadcast: %s', message)



bus = RmqServiceBus()
service = ThreeService('union_three', bus)


loop = asyncio.get_event_loop()
task = asyncio.ensure_future(service.start(loop))
loop.run_forever()