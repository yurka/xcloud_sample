
import asyncio

import logging.config
from union.bus.aio import RmqServiceBus
from union.service import Service, register


logging.config.fileConfig('logging.conf')


class BenchService(Service):

    @register()
    async def process(self, a, b, c):
        """
        Process data
        """
        # big = [ {'key %s' % i : 'value %s' % i for i in range(n)} for n in range(20)]
        return a, b, c


bus = RmqServiceBus(rmq_url="amqp://guest:1231234@localhost:5672/utest/")
service = BenchService('bench', bus)


loop = asyncio.get_event_loop()
task = asyncio.ensure_future(service.start(loop))
try:
    loop.run_forever()
except KeyboardInterrupt:
    bus.leave()
