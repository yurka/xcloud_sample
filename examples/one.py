
import asyncio
import itertools

import logging.config
from union.bus.aio import RmqServiceBus
from union.service import Service, register, subscribe

logging.config.fileConfig('logging.conf')

_words = itertools.cycle([
    "This",
    "function",
    "is",
    "primarily",
    "used",
    "as",
    "a",
    "transition",
    "tool",
    "for",
    "programs",
    "being",
    "converted"])


class OneService(Service):

    async def start(self, ioloop):
        await super().start(ioloop)
        self.log.info('UP!')
        asyncio.ensure_future(self.job())

    async def job(self):
        cnt = 0
        while True:
            word = _words.__next__() + ' # ' + str(cnt)
            self.log.debug('Try word %s', word)
            try:
                res = await self.bus.rpc_call('union_two.process_word', word, timeout=5)
                self.log.debug('Word: %s, Result: %s', word, res)
            except Exception as e:
                self.log.exception(e)
            # try:
            #     res = await self.bus.rpc_call('union_two.divide', 1, 2, timeout=5)
            # except ZeroDivisionError:
            #     print('Oops')
            # else:
            #     self.log.debug('Divide Result: %s', res)
            # await self.bus.publish('broadcast', 'Dada')
            # await asyncio.sleep(1)
            cnt += 1

    @register()
    def reverse(self, word):
        """
        Reverse given word
        """
        return word[::-1]


bus = RmqServiceBus()
service = OneService('union_one', bus)


loop = asyncio.get_event_loop()
task = asyncio.ensure_future(service.start(loop))
loop.run_forever()
