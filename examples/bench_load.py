
import asyncio
import time

from union.bus.aio import RmqServiceBus

cnt = 0
start = time.time()

DELAY = 0


def on_shutdown():
    print("Total messages:", cnt)
    print("Time:", time.time() - start)
    print("MPS:", cnt / (time.time() - start))


bus = RmqServiceBus('xxx', rmq_url="amqp://guest:1231234@localhost:5672/utest/")


async def run(loop):
    global cnt
    global start
    await bus.join(loop)
    start = time.time()
    while True:
        res = await bus.rpc_call('bench.process', 1, 2, 'hello world')
        # print(res)
        cnt += 1


loop = asyncio.get_event_loop()
task = asyncio.ensure_future(run(loop))
try:
    loop.run_forever()
except KeyboardInterrupt:
    task.cancel()
    on_shutdown()
