from union.bus.blocking import BlockingServiceBus


bus = BlockingServiceBus()
print('bus created')
bus.join()
print('bus joined')
print('calling "union_two.get_api_doc"')
res = bus.rpc_call('union_two.api_doc')
print('union_two.get_api_doc result:', res)
bus.publish('broadcast', 'a message')
print('pub ok')
# res = bus.rpc_call('union_two.divide', 1, 3)
# print('div', res)

