
import json
import asyncio

import logging.config
from union.bus.aio import RmqServiceBus
from union.service import Service, register, subscribe


logging.config.fileConfig('logging.conf')


class TwoService(Service):
    """
    Второй сервис
    """

    async def start(self, ioloop):
        await super().start(ioloop)
        self.log.info('UP!')
        # api_docs = await self.get_api_doc()
        # self.log.info('got api docs from "union_one": %s', api_docs)

    # async def job(self):
    #     while True:
    #         word = _words.__next__()
    #         res = await self.bus.rpc_call('union_two.process_word', word)
    #         self.log.info('Word: %s, Result: %s', word, res)

    @register()
    async def process_word(self, word):
        """
        Process given word
        """
        res = await self.bus.rpc_call('union_three.to_upper', word)
        # self.foobar
        return res

    @register()
    def divide(self, a, b):
        """
        Divide a/b
        """
        res = a/b
        return res

    @subscribe('printer')
    def process_msg(self, message):
        self.log.info('got message "printer": %s', message)

    @subscribe('broadcast')
    def listen_broadcast(self, message):
        self.log.info('Broadcast: %s', message)


bus = RmqServiceBus()
service = TwoService('union_two', bus)


loop = asyncio.get_event_loop()
task = asyncio.ensure_future(service.start(loop))
loop.run_forever()
