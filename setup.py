import os
import sys
from setuptools import setup, find_packages


PY_VER = sys.version_info

if PY_VER < (3, 5):
    raise RuntimeError("union doesn't suppport Python earlier than 3.5")


def read(f):
    return open(os.path.join(os.path.dirname(__file__), f)).read().strip()

install_requires = [
]

classifiers = [
    'License :: OSI Approved :: BSD License',
    'Intended Audience :: Developers',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.5',
    'Operating System :: POSIX',
    'Operating System :: MacOS :: MacOS X',
    'Environment :: Web Environment',
    'Development Status :: 4 - Beta',
    'Topic :: Microservices',
]


setup(name='union',
      version='0.1',
      description=('Microservices Framework.'),
      long_description='\n\n'.join(('Microservices Framework.', read('CHANGES.txt'))),
      classifiers=classifiers,
      platforms=['POSIX'],
      author='Khomyakov Yuriy',
      author_email='yuriy.homyakov@gmail.com',
      url='no',
      download_url='no',
      license='BSD',
      packages=find_packages(),
      install_requires=install_requires,
      include_package_data=True,
      entry_points={
          'console_scripts': ['ulaunch = union.scripts.launcher:main'],
      })
