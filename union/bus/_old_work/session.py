# -*- coding:utf-8 -*-

import pika
# import time
import uuid
import os

import json


# from pika import exceptions
from pika.adapters import twisted_connection
from twisted.internet import defer, reactor, protocol
from twisted.logger import Logger


class RmqAppSession(object):
    """
    Сессия сервиса - подключение к шинев
    """

    log = Logger()

    def __init__(self, host='localhost', port=5672):
        self.service_name = None
        self.host = host
        self.port = port
        self.conn = None
        self.channel = None
        self.rpc_channel = None
        self._rpc_queues = {}
        self._exchanges = set()
        self._response_waiters = {}
        self._rpc_endpoints_registry = {}
        self._registrations = {}
        self.ready = defer.Deferred()

    # @defer.inlineCallbacks
    def join(self):
        parameters = pika.ConnectionParameters()
        cc = protocol.ClientCreator(
            reactor,
            twisted_connection.TwistedProtocolConnection, parameters)
        d = cc.connectTCP(self.host, self.port)
        d.addCallback(lambda proto: proto.ready)
        d.addCallback(self._on_join)
        return self.ready

    def leave(self):
        pass

    def set_service(self, service):
        self.service_name = service.name

    @defer.inlineCallbacks
    def _on_join(self, conn):
        self.conn = conn
        self.channel = yield self.conn.channel()
        self.rpc_channel = yield self.conn.channel()
        yield self.rpc_channel.basic_qos(prefetch_count=1)
        yield self.channel.exchange_declare(exchange='ex1',
                                            type='direct')

        result = yield self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue

        self.register('apidocs', self.apidocs)
        self._wait_call_response()
        self.on_join()
        d, self.ready = self.ready, None
        if d:
            d.callback(True)

    @defer.inlineCallbacks
    def _wait_call_response(self):
        queue_object, consumer_tag = yield self.channel.basic_consume(
            queue=self.callback_queue, no_ack=True)
        while True:
            ch, method, props, body = yield queue_object.get()
            # print("Rersponse:", ch, method, props, body)
            try:
                msg = json.loads(body.decode())
                self._dispatch_response(props.correlation_id, msg)
            except Exception as e:
                self.log.error("ERR : {}".format(e))

    def _dispatch_response(self, correlation_id, msg):
        d = self._response_waiters.pop(correlation_id, None)
        if d is None:
            self.log.info('No waiters for response: {}#{}'.format(correlation_id, msg))
        else:
            d.callback(msg)

    @defer.inlineCallbacks
    def _init_exchange(self, name):
        if name not in self._exchanges:
            yield self.channel.exchange_declare(exchange=name,
                                                type='fanout')
            self._exchanges.add(name)

    def on_join(self):
        pass

    @defer.inlineCallbacks
    def register(self, endpoint, callable):
        """
        Register RPC endpoint
        """
        self._rpc_endpoints_registry[endpoint] = callable.__doc__
        endpoint = "{}.{}".format(self.service_name, endpoint)
        result = yield self.rpc_channel.queue_declare(queue=endpoint)
        # print "register: ", endpoint, callable
        self.log.info('RPC endpoint registered: {} {}'.format(endpoint, callable))
        def wrap(cb):
            def _wrapper(msg):
                args, kw = msg
                try:
                    ret = cb(*args, **kw)
                except Exception as e:
                    ret = {'exception': repr(e)}
                return ret
            return _wrapper

        self._consume(self.rpc_channel, endpoint, wrap(callable), need_response=True)

    def unregister(self, endpoint, callable):
        pass

    @defer.inlineCallbacks
    def call(self, endpoint, *args, **kw):
        request = json.dumps((args, kw))
        corr_id = str(uuid.UUID(bytes=os.urandom(16), version=4))
        d = defer.Deferred()
        self._response_waiters[corr_id] = d
        yield self.channel.basic_publish(
            exchange='',
            routing_key=endpoint,
            properties=pika.BasicProperties(reply_to=self.callback_queue, correlation_id=corr_id),
            body=request)
        res = yield d
        defer.returnValue(res)

    @defer.inlineCallbacks
    def subscribe(self, queue, callable):
        chan = self.channel
        result = yield chan.queue_declare(exclusive=True)
        yield self._init_exchange(queue)
        chan.queue_bind(exchange=queue,
                        queue=result.method.queue)
        self._consume(chan, result.method.queue, callable)
        self.log.info('SUBSCRIBE on {} -> {}'.format(queue, callable))

    @defer.inlineCallbacks
    def _consume(self, channel, queue, callable, need_response=False):
        queue_object, consumer_tag = yield channel.basic_consume(queue=queue, no_ack=False)
        # print "Consume:", queue, queue_object, consumer_tag, callable
        while True:
            ch, method, props, body = yield queue_object.get()
            # print("Got:", ch, method, props, body)
            try:
                msg = json.loads(body.decode())
                response = callable(msg)
                response = json.dumps(response)
                if need_response:
                    yield channel.basic_publish(
                        exchange='',
                        routing_key=props.reply_to,
                        properties=pika.BasicProperties(
                            correlation_id=props.correlation_id),
                        body=response)
                    # yield channel.basic_ack(delivery_tag=method.delivery_tag)
            except Exception as e:
                self.log.error("ERR : {}".format(e))
            else:
                yield ch.basic_ack(delivery_tag=method.delivery_tag)

    def unsubscribe(self, queue, callable):
        pass

    @defer.inlineCallbacks
    def publish(self, queue, message):
        body = json.dumps(message)
        chan = self.channel
        yield self._init_exchange(queue)
        # print 'publish {} on {}'.format(message, queue)
        yield chan.basic_publish(exchange=queue, routing_key='', body=body)

    def apidocs(self):
        """
        Returns full api documentation for RPC endpoints and cnannel subscribers,
        registered in session
        """
        rpc = [{"name": name, "doc": doc}
               for name, doc in self._rpc_endpoints_registry.items()]
        subs = []
        docs = {'service_name': self.service_name, 'rpc': rpc, "subscribers": subs}
        return docs

    def dec_register(self, endpoint=None):
        def _wrapper(func):
            key = endpoint or func.__name__
            self._registrations[key] = func
            return func
        return _wrapper