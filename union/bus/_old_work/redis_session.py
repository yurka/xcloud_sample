import sys
import json
import uuid
from collections import defaultdict

from twisted.internet import reactor, protocol, defer
from twisted.logger import Logger
from txredis.client import RedisClient, RedisSubscriber

REDIS_HOST = 'localhost'
REDIS_PORT = 6379


class Subscriber(RedisSubscriber):

    log = Logger()

    def set_message_handler(self, handler):
        self.message_handler = handler

    def messageReceived(self, channel, message):
        self.log.info("CH {} --> {}".format(channel, message))
        self.message_handler(channel, message)

def getRedisSubscriber(REDIS_HOST, REDIS_PORT):
    clientCreator = protocol.ClientCreator(reactor, Subscriber)
    return clientCreator.connectTCP(REDIS_HOST, REDIS_PORT)


def getRedis(REDIS_HOST, REDIS_PORT):
    clientCreator = protocol.ClientCreator(reactor, RedisClient)
    return clientCreator.connectTCP(REDIS_HOST, REDIS_PORT)


class RedisAppSession(object):
    """
    """

    log = Logger()

    def __init__(self, host='localhost', port=6379, serializer='json'):
        self.serializer = json
        self._subscriptions = defaultdict(list)
        self._waiters = dict()
        self._rpc_endpoints = dict()
        self.init(host, port)

    @defer.inlineCallbacks
    def init(self, host, port):
        self.subscriber = yield getRedisSubscriber(host, port)
        self.client = yield getRedis(host, port)
        self.callback_client = yield getRedis(host, port)
        self.rpc_client = yield getRedis(host, port)
        self.subscriber.set_message_handler(self.on_message)
        self._on_join()

    def _on_join(self):
        self.log.info('Joined!')
        self.callback_queue = "callback_queue-{}".format(uuid.uuid4())
        self._wait_callback(self.callback_queue)
        print 'Joined!'
        self.on_join()

    def on_join(self):
        pass

    def on_message(self, channel, message):
        for cb in self._subscriptions[channel]:
            cb(message)

    @defer.inlineCallbacks
    def publish(self, channel, message):
        data = self.serializer.dumps(message)
        res = yield self.client.publish(channel, data)

    @defer.inlineCallbacks
    def subscribe(self, channel, callable):
        response = 1
        if channel not in self._subscriptions:
            response = yield self.subscriber.subscribe(channel)
        self._subscriptions[channel].append(callable)
        defer.returnValue(response)

    @defer.inlineCallbacks
    def call(self, endpoint, *args, **kw):
        corr_id = str(uuid.uuid4())
        request = {
            '_meta': {
                'correlation_id': corr_id,
                'reply_to': self.callback_queue
            },
            'payload': {
                'args': args,
                'kwargs': kw
            }
        }
        data = self.serializer.dumps(request)
        d = defer.Deferred()
        self._waiters[corr_id] = d
        res = yield self.client.rpush(endpoint, data)
        res = yield d
        defer.returnValue(res)

    def register(self, endpoint, callable):
        if endpoint in self._rpc_endpoints:
            raise RuntimeError('RPC endpoint {} already registered'.format(endpoint))
        else:
            self._wait_rpc_call(endpoint, callable)

    @defer.inlineCallbacks
    def _wait_callback(self, list_name):
        print "wait cb", list_name
        while True:
            chan, message = yield self.callback_client.bpop([list_name])
            # print "Data from tx1: {}".format(message)
            response = self.serializer.loads(message)
            corr_id = response['_meta']['correlation_id']
            d = self._waiters.pop(corr_id, None)
            if d is not None:
                d.callback(response['payload'])
            else:
                print 'miss', corr_id
                d.callback('miss')
                # self.log.info('No waiter for {}'.format(corr_id))

    @defer.inlineCallbacks
    def _wait_rpc_call(self, list_name, callable):
        print "wait rpc", list_name, callable
        while True:
            chan, message = yield self.rpc_client.bpop([list_name])
            response = self.serializer.loads(message)
            corr_id = response['_meta']['correlation_id']
            reply_to = response['_meta']['reply_to']
            res = callable(*response['payload']['args'], **response['payload']['kwargs'])
            response = {
                "_meta": {
                    "correlation_id": corr_id
                },
                "payload": res
            }
            yield self.client.rpush(reply_to, self.serializer.dumps(response))