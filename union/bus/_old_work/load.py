# -*- coding:utf-8 -*-

import time
import session

from twisted.internet import defer, reactor, task


cnt = 0
start = time.time()

DELAY = 0


def on_shutdown():
    print("Total messages:", cnt)
    print("Time:", time.time() - start)
    print("MPS:", cnt / (time.time() - start))


class LoadSession(session.RmqAppSession):

    def on_join(self):
        print('on join s')
        # task.LoopingCall(self.publish, 'xqueue', 'abc').start(1)
        # task.LoopingCall(self.publish, 'xqueue', {"a": [1, 2, 3]}).start(2)

        # @defer.inlineCallbacks
        # def call_rpc(endpoint):
        #     global cnt
        #     cnt += 1
        #     # print 'PUB {}'.format('queue_a')
        #     # yield self.publish('queue_a', {'msg': 'Asdasdasd', 'cnt': cnt})
        #     # print('RPC Call: {}'.format(endpoint))

        #     # l = [self.call(endpoint, i, cnt, b='B', x=42) for i in xrange(10)]
        #     # dl = defer.DeferredList(l)

        #     # res = yield dl
        #     res = yield self.call(endpoint, 0, cnt, b='B', x=42)
        #     # res = None
        #     print('RPC Response: {} --> {}'.format(endpoint, res))

        #     reactor.callLater(DELAY, call_rpc, 'worker.method_b')

        # # for x in range(100):
        # reactor.callLater(0, call_rpc, 'worker.method_b')


@defer.inlineCallbacks
def run():
    print ('xx')
    sess = LoadSession(host='localhost', port=5672)
    yield sess.join()
    global cnt
    while True:
        endpoint = 'worker.method_b'
        res = yield sess.call(endpoint, 0, cnt, b='B', x=42)
        # sess.publish('queue_a', {'b':'B', 'x':42})
        res = None
        cnt += 1
        # print('RPC Response: {} --> {}'.format(endpoint, res))

    # def foo():
    #     global cnt
    #     cnt += 1
    #     sess.publish('queue_a', 'abc')
        
    # task.LoopingCall(foo).start(0)
    print ('xx33')


sess = LoadSession(host='localhost', port=5672)
reactor.callWhenRunning(run)

reactor.addSystemEventTrigger("before", "shutdown", on_shutdown)
reactor.run()
