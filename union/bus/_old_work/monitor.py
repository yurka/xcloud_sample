from twisted.web import server, resource
from twisted.internet import reactor, endpoints, defer

import session


# class MonitorSession(session.RmqAppSession):

#     def on_join(self):
#         # task.LoopingCall(self.publish, 'xqueue', 'abc').start(1)
#         # task.LoopingCall(self.publish, 'xqueue', {"a": [1, 2, 3]}).start(2)
#         pass

#         # @defer.inlineCallbacks
#         # def call_rpc(endpoint, *args, **kw):
#         #     # print 'RPC Call: {}({})'.format(endpoint, (args, kw))
#         #     global cnt
#         #     cnt += 1
#         #     res = yield self.call(endpoint, cnt, *args, **kw)
#         #     # print 'RPC Response: {}({}) --> {}'.format(endpoint, (args, kw), res)
#         #     reactor.callLater(0, call_rpc, 'com.txbus.test', 'A', 77, b='B', x=42)

#         # reactor.callLater(0, call_rpc, 'com.txbus.test', 'A', 77, b='B', x=42)


sess = session.RmqAppSession()
sess.join()


class GenResource(resource.Resource):

    def render(self, request):
        resp = resource.Resource.render(self, request)
        if isinstance(resp, defer.Deferred):
            resp.addCallback(request.write)

            def finish(_):
                if not request._disconnected and not request.finished:
                    request.finish()

            resp.addCallback(finish)

            return server.NOT_DONE_YET
        return resp


class Docs(GenResource):
    isLeaf = True

    @defer.inlineCallbacks
    def render_GET(self, request):
        request.setHeader("content-type", "text/plain")
        resp = yield sess.call('worker.apidocs')
        defer.returnValue("Got {}".format(resp))


class TestA(GenResource):
    isLeaf = True

    @defer.inlineCallbacks
    def render_GET(self, request):
        request.setHeader("content-type", "text/plain")
        resp = yield sess.call('worker.method_a', **request.args)
        defer.returnValue("Got {}".format(resp))


class TestB(GenResource):
    isLeaf = True

    @defer.inlineCallbacks
    def render_GET(self, request):
        request.setHeader("content-type", "text/plain")
        resp = yield sess.call('worker.method_b', **request.args)
        defer.returnValue("Got {}".format(resp))


class TestC(GenResource):
    isLeaf = True

    @defer.inlineCallbacks
    def render_GET(self, request):
        request.setHeader("content-type", "text/plain")
        resp = yield sess.publish('queue_a', request.args)
        defer.returnValue("Got {}".format(resp))



root = resource.Resource()
root.putChild("docs", Docs())
root.putChild("method_a", TestA())
root.putChild("method_b", TestB())
root.putChild("pub", TestC())

endpoints.serverFromString(reactor, "tcp:8080").listen(server.Site(root))

reactor.run()
