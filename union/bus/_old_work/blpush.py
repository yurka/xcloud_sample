import sys

import blocking_session


sess = blocking_session.BlockingSession()
sess.join()


sess.publish('queue_a', sys.argv[1:])
res = sess.call('worker.method_b', sys.argv[1:])

print('->', res)
