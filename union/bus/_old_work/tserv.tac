# -*- coding:utf-8 -*-

from twisted.application import service

import session
import tserv


application = service.Application("WorkerExample")
# sess = session.RmqAppSession(host='146.148.22.33', port=5672)
# worker = tserv.WorkerService()
sess = session.RmqAppSession(host='localhost', port=5672)
worker = tserv.WorkerService(session=sess)
worker.setServiceParent(application)

# import Manhole
# manhole = Manhole(port=1234)
# manhole.setServiceParent(application)


# import Web
# web = Web(port=8888)
# web.setServiceParent(application)
