from twisted.internet import reactor, protocol, defer
from twisted.python import log

from txredis.client import RedisClient, RedisSubscriber

import sys

REDIS_HOST = 'localhost'
REDIS_PORT = 6379


class Service(RedisSubscriber):

    def messageReceived(self, channel, message):
        log.msg("CH {} --> {}".format(channel, message))


class Cli(RedisClient):

    def _send(self, *args, **kw):
        log.msg("SENDING: {}, {}".format(args, kw))
        RedisClient._send(self, *args, **kw)
        log.msg("SENT")

    def messageReceived(self, channel, message):
        log.msg("CHX {} --> {}".format(channel, message))


def getRedisSubscriber():
    clientCreator = protocol.ClientCreator(reactor, Service)
    return clientCreator.connectTCP(REDIS_HOST, REDIS_PORT)


def getRedis():
    clientCreator = protocol.ClientCreator(reactor, Cli)
    return clientCreator.connectTCP(REDIS_HOST, REDIS_PORT)


@defer.inlineCallbacks
def runTest():
    redis1 = yield getRedisSubscriber()
    redis2 = yield getRedis()

    log.msg("redis1: SUBSCRIBE w00t")
    response = yield redis1.subscribe("w00t")
    response = yield redis1.subscribe("w00t2")
    log.msg("subscribed to w00t, response = %r" % response)

    log.msg("redis2: PUBLISH w00t 'Hello, world!'")
    response = yield redis2.publish("w00t", "Hello, world!")
    log.msg("published to w00t, response = %r" % response)

    while True:
        chan, data = yield redis2.bpop(["tx1"])
        log.msg("Data from tx1: {}".format(data))
        if data == 'x':
            reactor.stop()


def main():
    log.startLogging(sys.stdout)
    reactor.callLater(0, runTest)
    reactor.run()


if __name__ == "__main__":
    main()
