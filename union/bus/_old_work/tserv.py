# -*- coding:utf-8 -*-
"""
Docs docs
"""

from twisted.internet import defer, reactor, protocol, task
from twisted.application import internet, service
from twisted.logger import Logger

import session


sess = session.RmqAppSession(host='localhost', port=5672)


def register(endpoint=None):
    def _wrapper(func):
            name = endpoint or func.__name__
            func.__bus_register_params__ = {'name': name, 'method': func.__name__, 'type': 'rpc'}
            return func
    return _wrapper


def subscribe(channal=None):
    def _wrapper(func):
            name = channal or func.__name__
            func.__bus_register_params__ = {'name': name, 'method': func.__name__, 'type': 'sub'}
            return func
    return _wrapper


def pull(endpoint=None):
    def _wrapper(func):
            name = endpoint or func.__name__
            func.__bus_register_params__ = {'name': name, 'method': func.__name__, 'type': 'pull'}
            return func
    return _wrapper


class ServiceMeta(type):
    """
    Метакласс Сервиса - для отложенной регистрации на шине.
    """

    def __new__(cls, name, bases, dct):
        ncl = type.__new__(cls, name, bases, dct)
        ncl.__bus_registrations__ = {}
        for name in dir(ncl):
            method = getattr(ncl, name, None)
            if callable(method) and hasattr(method, '__bus_register_params__'):
                params = method.__bus_register_params__
                ncl.__bus_registrations__[params['name']] = params
        return ncl


# class ServiceBase(service.Service):
#     """
#     Базовый класс для сервиса

#     Реализует запуск-остановку сервиса и регистрацию оьраьотчиков на шине.
#     """
#     __metaclass__ = ServiceMeta
class ServiceBase(service.Service, metaclass=ServiceMeta):
    """
    Базовый класс для сервиса

    Реализует запуск-остановку сервиса и регистрацию оьраьотчиков на шине.
    """
    __metaclass__ = ServiceMeta

    log = Logger()

    def on_connected(self, flag):
        self._apply_registrations()

    def _apply_registrations(self):
        for name, params in self.__bus_registrations__.items():
            if params['type'] == 'rpc':
                self.session.register(name, getattr(self, params['method']))
            elif params['type'] == 'sub':
                self.session.subscribe(name, getattr(self, params['method']))
            elif params['type'] == 'pull':
                self.session.pull(name, getattr(self, params['method']))

    def startService(self):
        d = self.session.join()
        d.addCallback(self.on_connected)

    def stopService(self):
        self.session.leave()


class WorkerService(ServiceBase):
    """
    Demo Worker service
    """

    name = "worker"


    def __init__(self, session=None):
        self.session = session
        session.set_service(self)

    @register()
    def method_a(self, *args, **kw):
        """
        This is a test PRC endpoint

        Args:
            args: positional params
            kw (dict): keyword params
        Returns:
            A dict mapping with blah-blah
        Raises:
            IndexError

        И немного по-русски...
        """
        # print('method_a was called with:', args, kw)
        return {'id': args[0], 'args': args, 'kw': kw}

    @register('method_b')
    def method_b_endpoint(self, *args, **kw):
        """
        This is a test PRC endpoint

        Args:
            args: positional params
            kw (dict): keyword params
        Returns:
            A dict mapping with blah-blah
        Raises:
            Nothing yet
        """
        # print('method_b was called with:', args, kw)
        return {'args': args, 'kw': kw}

    @subscribe('queue_a')
    def queue_a_handler(self, msg):
        """
        Обработчик для сообщений очереди 'queue_a'

        Печатает полученное сообщение в консоль

        Args:
            msg (dict): ожидает соощение формата {...}
        Raises:
            RuntimeError
            KeyError
        """
        print('Message from queue_a:', msg)
