# -*- coding:utf-8 -*-

import time
import redis_session

from twisted.internet import defer, reactor, task


cnt = 0
start = None

def on_shutdown():
    print "Total messages:", cnt
    print "Time:", time.time() - start
    print "MPS:", cnt / (time.time() - start)


class LoadSession(redis_session.RedisAppSession):

    def on_join(self):
        print 'AA'

        global start
        start = time.time()
        # def publish(ch, msg):
        #     global cnt
        #     self.publish(ch, msg)
        #     cnt += 1

        # task.LoopingCall(publish, 'xqueue', 'abc').start(0)
        # task.LoopingCall(publish, 'xqueue', {"a": [1, 2, 3]}).start(0.1)

        @defer.inlineCallbacks
        def call_rpc(endpoint, *args, **kw):
            # print 'RPC Call: {}({})'.format(endpoint, (args, kw))
            global cnt
            cnt += 1
            res = yield self.call(endpoint, cnt, *args, **kw)
            # print 'RPC Response: {}({}) --> {}'.format(endpoint, (args, kw), res)
            reactor.callLater(0, call_rpc, 'com.txbus.test', 'A', 77, b='B', x=42)

        reactor.callLater(0, call_rpc, 'com.txbus.test', 'A', 77, b='B', x=42)

        # print 'BB'

sess = LoadSession()


reactor.addSystemEventTrigger("before", "shutdown", on_shutdown)
reactor.run()
