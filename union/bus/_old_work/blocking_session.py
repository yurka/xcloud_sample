# -*- coding:utf-8 -*-

import pika
import uuid
import os

import json

from logging import getLogger


class BlockingSession(object):
    """
    Сессия сервиса - подключение к шине
    Блокирующий вариант, для WSGI и консоли
    """

    log = getLogger('bus')

    def __init__(self, host='localhost', port=5672):
        self.service_name = None
        self.host = host
        self.port = port
        self.conn = None
        self.channel = None
        self._exchanges = set()
        self._response_waiters = {}
        self._rpc_endpoints_registry = {}
        self._registrations = {}

    def join(self):
        parameters = pika.ConnectionParameters(
            host=self.host
        )
        self.conn = pika.BlockingConnection(parameters)
        self.channel = self.conn.channel()

        self.channel.exchange_declare(exchange='ex1',
                                      type='direct')

        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue

        self.on_join()

    def leave(self):
        pass

    def on_join(self):
        pass

    def set_service(self, service):
        self.service_name = service.name

    def _wait_call_response(self, corr_id):

        def callback(ch, method, props, body):
            # print("Rersponse:", ch, method, props, body)
            try:
                msg = json.loads(body.decode())
                self._response_waiters[props.correlation_id] = msg
            except Exception as e:
                self.log.error("ERR : {}".format(e))
            finally:
                self.channel.stop_consuming()

        self.channel.basic_consume(
            callback,
            queue=self.callback_queue,
            no_ack=True)
        self.channel.start_consuming()

    def _init_exchange(self, name):
        if name not in self._exchanges:
            yield self.channel.exchange_declare(exchange=name,
                                                type='fanout')
            self._exchanges.add(name)

    def call(self, endpoint, *args, **kw):
        request = json.dumps((args, kw))
        corr_id = str(uuid.UUID(bytes=os.urandom(16), version=4))
        self.channel.basic_publish(
            exchange='',
            routing_key=endpoint,
            properties=pika.BasicProperties(reply_to=self.callback_queue, correlation_id=corr_id),
            body=request)
        self._wait_call_response(corr_id)
        return self._response_waiters[corr_id]

    def publish(self, queue, message):
        body = json.dumps(message)
        chan = self.channel
        self._init_exchange(queue)
        # print 'publish {} on {}'.format(message, queue)
        chan.basic_publish(exchange=queue, routing_key='', body=body)
