import sys

from twisted.internet import reactor, defer

import session


sess = session.RmqAppSession()
d = sess.join()


@defer.inlineCallbacks
def pub(a):
    r = yield sess.publish("queue_a", {'a': 12, 'args': sys.argv[1:]})
    print('/pub 1', r)
    res = yield sess.call('worker.method_b', sys.argv[1:])
    print('/pub 2', res)
    # reactor.stop()
    reactor.callLater(0.01, reactor.stop)


# # pub/sub
# sess.publish
# sess.subscribe

# # rpc
# sess.call
# sess.register

# # init
# sess.join
# sess.leave


def stop(b):
    print('/stop', b)
    reactor.callLater(1, reactor.stop)

d.addCallback(pub)
# d.addCallback(stop)
# reactor.callWhenRunning(pub, 1)
reactor.run()
