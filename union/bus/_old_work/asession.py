# -*- coding:utf-8 -*-

import time
import uuid
import os
import logging
import json
import asyncio
from functools import partial

# from pika import exceptions
import pika
from pika.adapters import asyncio_connection


class RmqAppSession(object):
    """
    Сессия сервиса - подключение к шинев
    """

    log = logging.getLogger('aiosess')

    def __init__(self, host='localhost', port=5672):
        self.service_name = None
        self.host = host
        self.port = port
        self.conn = None
        self.channel = None
        self.rpc_channel = None
        self._rpc_queues = {}
        self._exchanges = set()
        self._response_waiters = {}
        self._rpc_endpoints_registry = {}
        self._registrations = {}
        self._respose_waiter = None
        self.ready = asyncio.Future()

    @asyncio.coroutine
    def join(self):
        parameters = pika.ConnectionParameters()
        ioloop = asyncio.get_event_loop()
        coro = ioloop.create_connection(
            lambda: asyncio_connection.AsyncioProtocolConnection(parameters, ioloop),
            self.host,
            self.port
        )
        transport, protocol = yield from asyncio.ensure_future(coro)
        print('Create conn:', transport, protocol)
        self.conn = protocol
        yield from self.conn.ready
        yield from asyncio.ensure_future(self._on_join())
        print('KISS!')
        return self.ready

    def leave(self):
        self._respose_waiter.cancel()

    def set_service(self, service):
        self.service_name = service.name

    @asyncio.coroutine
    def _on_join(self):
        self.channel = yield from self.conn.channel()
        self.rpc_channel = yield from self.conn.channel()
        yield from self.rpc_channel.basic_qos(prefetch_count=1)
        yield from self.channel.exchange_declare(exchange='ex1',
                                            type='direct')

        result = yield from self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue

        # self.register('apidocs', self.apidocs)

        self._respose_waiter = asyncio.ensure_future(self._wait_call_response())
        print('ttt')

    @asyncio.coroutine
    def _wait_call_response(self):
        queue_object, consumer_tag = yield from self.channel.basic_consume(
            queue=self.callback_queue, no_ack=True)

        while True:
            ch, method, props, body = yield from queue_object.get()
            # print("Rersponse:", ch, method, props, body)
            try:
                msg = json.loads(body.decode())
                self._dispatch_response(props.correlation_id, msg)
            except Exception as e:
                self.log.error("ERR : {}".format(e))

    def _dispatch_response(self, correlation_id, msg):
        d = self._response_waiters.pop(correlation_id, None)
        if d is None:
            self.log.info('No waiters for response: {}#{}'.format(correlation_id, msg))
        else:
            d.set_result(msg)

    @asyncio.coroutine
    def _init_exchange(self, name):
        if name not in self._exchanges:
            yield from self.channel.exchange_declare(exchange=name,
                                                     type='fanout')
            self._exchanges.add(name)

    def on_join(self):
        pass

    @asyncio.coroutine
    def register(self, endpoint, callable):
        """
        Register RPC endpoint
        """
        self._rpc_endpoints_registry[endpoint] = callable.__doc__
        endpoint = "{}.{}".format(self.service_name, endpoint)
        result = yield from self.rpc_channel.queue_declare(queue=endpoint)
        self.log.info('RPC endpoint registered: {} {}'.format(endpoint, callable))
        def wrap(cb):
            def _wrapper(msg):
                args, kw = msg
                try:
                    ret = cb(*args, **kw)
                except Exception as e:
                    ret = {'exception': repr(e)}
                return ret
            return _wrapper

        self._consume(self.rpc_channel, endpoint, wrap(callable), need_response=True)

    def unregister(self, endpoint, callable):
        pass

    @asyncio.coroutine
    def call(self, endpoint, *args, **kw):
        request = json.dumps((args, kw))
        corr_id = str(uuid.UUID(bytes=os.urandom(16), version=4))
        d = asyncio.Future()
        self._response_waiters[corr_id] = d
        yield from self.channel.basic_publish(
            exchange='',
            routing_key=endpoint,
            properties=pika.BasicProperties(reply_to=self.callback_queue, correlation_id=corr_id),
            body=request)
        print('in call    ', time.time())
        res = yield from d
        return res
        # return d

    @asyncio.coroutine
    def subscribe(self, queue, callable):
        chan = self.channel
        result = yield from chan.queue_declare(exclusive=True)
        yield from self._init_exchange(queue)
        chan.queue_bind(exchange=queue,
                        queue=result.method.queue)
        self._consume(chan, result.method.queue, callable)
        self.log.info('SUBSCRIBE on {} -> {}'.format(queue, callable))

    @asyncio.coroutine
    def _consume(self, channel, queue, callable, need_response=False):
        queue_object, consumer_tag = yield from channel.basic_consume(queue=queue, no_ack=False)
        # print "Consume:", queue, queue_object, consumer_tag, callable
        while True:
            ch, method, props, body = yield from queue_object.get()
            # print "Got:", ch, method, props, body
            try:
                msg = json.loads(body.decode())
                response = callable(msg)
                response = json.dumps(response)
                if need_response:
                    yield from channel.basic_publish(
                        exchange='',
                        routing_key=props.reply_to,
                        properties=pika.BasicProperties(
                            correlation_id=props.correlation_id),
                        body=response)
                    # yield from channel.basic_ack(delivery_tag=method.delivery_tag)
            except Exception as e:
                self.log.error("ERR : {}".format(e))
            else:
                yield from ch.basic_ack(delivery_tag=method.delivery_tag)

    def unsubscribe(self, queue, callable):
        pass

    @asyncio.coroutine
    def publish(self, queue, message):
        body = json.dumps(message)
        chan = self.channel
        yield from self._init_exchange(queue)
        # print 'publish {} on {}'.format(message, queue)
        yield from chan.basic_publish(exchange=queue, routing_key='', body=body)

    def apidocs(self):
        """
        Returns full api documentation for RPC endpoints and cnannel subscribers,
        registered in session
        """
        rpc = [{"name": name, "doc": doc}
               for name, doc in self._rpc_endpoints_registry.items()]
        subs = []
        docs = {'service_name': self.service_name, 'rpc': rpc, "subscribers": subs}
        return docs

    @asyncio.coroutine
    def push(self, endpoint, *args, **kw):
        request = json.dumps((args, kw))
        yield from self.channel.basic_publish(
            exchange='',
            routing_key=endpoint,
            body=request)