# -*- coding:utf-8 -*-

import time
import sys

import blocking_session


cnt = 0
start = time.time()

DELAY = 0


def on_shutdown():
    print("Total messages:", cnt)
    print("Time:", time.time() - start)
    print("MPS:", cnt / (time.time() - start))


sess = blocking_session.BlockingSession()
sess.join()


sess.publish('queue_a', sys.argv[1:])
res = sess.call('worker.method_b', sys.argv[1:])

try:
    while True:
        # sess.publish('queue_a', sys.argv[1:])
        cnt += 1
        res = sess.call('worker.method_b', sys.argv[1:])
        # print(res)
        time.sleep(DELAY)
except KeyboardInterrupt:
    on_shutdown()
