
from twisted.internet import reactor
from twisted.internet.defer import *

import zookeeper
from txzookeeper import ZookeeperClient


@inlineCallbacks
def test():
    print 'start'
    zk = ZookeeperClient(servers='127.0.0.1:2181', session_timeout=1000)
    print 'try to connect'
    yield zk.connect()
    print 'connected'
    res = yield zk.create('/foo', 'foodata', flags=zookeeper.EPHEMERAL)
    print 'created', res
    print 'wait exists'
    res, d = zk.exists_and_watch('/foo')
    res = yield res
    print 'wait', res
    res = yield d
    print 'modified', res
    # reactor.st

reactor.callWhenRunning(test)

reactor.run()
