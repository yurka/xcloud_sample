
import sys
import time
import asyncio
import logging

import asession
import pika
from pika.adapters import asyncio_connection

logging.basicConfig(level=logging.DEBUG)


DELAY = 0
cnt = 0
start = time.time()


def on_shutdown():
    print("Total messages:", cnt)
    print("Time:", time.time() - start)
    print("MPS:", cnt / (time.time() - start))


sess = asession.RmqAppSession()

ioloop = asyncio.get_event_loop()

print('!', sess, ioloop)


@asyncio.coroutine
def run(loop):
    global cnt
    global start
    yield from sess.join()
    start = time.time()
    r = yield from sess.publish('queue_a', sys.argv[1:])
    print('R:', r)

    while True:
        r = yield from sess.call('worker.method_b', a='ABC'*1500)
        # r = yield from sess.publish('queue_a', sys.argv[1:])
        # r = yield from sess.push('zq', sys.argv[1:])
        cnt += 1
        # print('R2:', r)

    sess.leave()


@asyncio.coroutine
def pull(ioloop):
    global cnt
    global start

    parameters = pika.ConnectionParameters()
    ioloop = asyncio.get_event_loop()
    coro = ioloop.create_connection(
        lambda: asyncio_connection.AsyncioProtocolConnection(parameters, ioloop),
        'localhost',
        5672
    )
    transport, conn = yield from asyncio.ensure_future(coro)
    yield from conn.ready
    chan = yield from conn.channel()
    start = time.time()
    queue_object, consumer_tag = yield from chan.basic_consume(
            queue='zq', no_ack=True)

    while True:
        ch, method, props, body = yield from queue_object.get()
        cnt += 1
        print(body)



try:
    ioloop.run_until_complete(run(ioloop))
    # ioloop.run_until_complete(pull(ioloop))
except KeyboardInterrupt:
    sess.leave()
finally:
    on_shutdown()
    print('Done')
