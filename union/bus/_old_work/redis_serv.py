# -*- coding:utf-8 -*-
import redis_session

from twisted.internet import defer, reactor, protocol, task


class WorkerSession(redis_session.RedisAppSession):

    def on_join(self):
        # task.LoopingCall(self.publish, 'xqueue', 'abc').start(1)

        def on_message(msg):
            print 'received:', msg
            pass

        def on_message_up(msg):
            print 'REICIVED:', msg
            pass

        self.subscribe('xqueue', on_message)
        self.subscribe('xqueue', on_message_up)

        def test_rpc(*args, **kw):
            # print 'test_rpc was called with:', args, kw
            return {'a': 12, 'args': args, 'kw': kw}

        self.register('com.txbus.test', test_rpc)


sess = WorkerSession()
reactor.run()
