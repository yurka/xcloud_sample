# coding: utf8

import uuid
import importlib

try:
    import ujson as json
except ImportError:  # no ujson
    import json


class RemoteException(Exception):
    """
    Ошибка в удаленном сервисе, которую не получилось транслировать через шину
    """


class BaseServiceBus:
    """
    Базовый класс сервисной шины.

    Сервисная шина позволяет микросервисам общаться между собой,
    используя паттерны RPC, PUB/SUB

    Решает вопросы service lookup и load ballancing.
    """

    _bus_prefix = "_uc"
    default_rpc_timeout = 3

    def __init__(self, service_name):
        self.service_name = service_name

    async def join(self):
        """
        Подключение к брокеру
        """

    def set_service(self, service):
        self.service_name = service.name
        self.service = service

    async def rpc_register(self, name, method):
        """
        Регистрация RPC метода
        """

    async def rpc_call(self, name, *args, **kw):
        """
        Вызов RPC метода.
        """

    async def publish(self, channel, message):
        """
        Публикация в PUB/SUB канал.
        """

    async def subscribe(self, channel, handler):
        """
        Регистрация обработчика сообщения в PUB/SUB канале.
        """

    def serialize(self, data):
        """
        Сериализация питон-объекта для передачи в шину
        """
        return json.dumps(data)

    def deserialize(self, msg):
        """
        Десериализация сообщения из шины в питон-объект
        """
        return json.loads(msg)

    def make_exception_response(self, exc):
        """
        Конвертировать Exception exc в сообщение которое можно отправить в шину.
        Это сообщение может быть обратно преобразовано в Exception
        и зарайзено методом maybe_raise_exception
        """
        eclass = exc.__class__
        msg = {
            "_exception": eclass.__name__,
            "_module": eclass.__module__,
            "_args": exc.args,
            "_repr": repr(exc),
        }
        return msg

    def maybe_raise_exception(self, msg):
        """
        Конвертировать сообщение с Exception в реальный Exception.
        Если в сообщении передан Exception - делает raise, а если нет - то нет
        """
        if not isinstance(msg, dict) or '_exception' not in msg:
            return

        try:
            module = importlib.import_module(msg['_module'])
            eclass = getattr(module, msg['_exception'])
            exc = eclass(*msg['_args'])
        except:
            exc = RemoteException(msg['_repr'])
        finally:
            raise exc

    def _generate_id(self):
        return str(uuid.uuid4()).split('-')[1]

    def _ensure_bus_prefix(self, name):
        if name.startswith(self._bus_prefix):
            return name
        else:
            return ".".join([self._bus_prefix, name])
