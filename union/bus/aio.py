# -*- coding:utf-8 -*-

import uuid
import logging
import time
import asyncio

import pika
from pika.adapters import asyncio_connection
from .base import BaseServiceBus


DEFAULT_CONNECT_URL = "amqp://guest:guest@localhost:5672/%2F"


async def make_connection(loop, url=None, params=None):
    """
    Создает соединение с RabbitMQ сервером

    Args:
        loop: asyncio IOloop.
        url (str): URL для соединения с RMQ
        params (pika.ConnectionParams): параметры Pika для соединения с RMQ.
    """
    if url is None:
        url = DEFAULT_CONNECT_URL
    if params is None:
        params = pika.URLParameters(url)

    def connection_factory():
        return asyncio_connection.AsyncioProtocolConnection(params, loop=loop)

    transport, connection = await loop.create_connection(connection_factory,
                                                         params.host,
                                                         params.port)
    await connection.ready
    return connection


class RmqServiceBus(BaseServiceBus):
    """
    Реализация сервисной шины для на основе RabbitMQ

    Args:
        service_name (str): Имя сервиса на шине - обычно устанавливается сервисом.
        rmq_url (str):   URL для соединения с RMQ брокером шины.
        rmq_params (pika.ConnectionParams): параметры Pika для соединения с
            RMQ брокером шины.

    Attributes:
        id: уникальный идентификатор на шине.
        conn: соединение с брокером.
        channel: общий канал в брокер.
        rpc_channel: канал, в котором принимаем RPC-запросы
    """

    log = logging.getLogger('service_bus')
    RECONNECT_INTERVAL = 1

    def __init__(self, service_name=None, rmq_url=None, rmq_params=None):
        self.service_name = service_name
        self.rmq_url = rmq_url
        self.rmq_params = rmq_params
        self.conn = None
        self.channel = None
        self.rpc_channel = None
        self.pull_channel = None
        self._exchanges = set()
        self._response_waiters = {}
        self._rpc_endpoints_registry = {}
        self._registrations = {}
        self._response_wait_task = None
        self.id = self._generate_id()
        self._connecting = False
        self._leaving = False
        self._consume_tasks = []
        self.pull_prefetch_count = None

    async def join(self, ioloop=None):
        """
        Подключение к брокеру.
        """
        self.log.info('Joining...')
        if self.service_name is None:
            raise RuntimeError("Can't join when ServiceBus.service_name is not defined.")
        n = 0
        while True:
            try:
                n += 1
                self.conn = await make_connection(ioloop,
                                                  url=self.rmq_url,
                                                  params=self.rmq_params)
            except Exception as e:
                self.log.warn('Connect error. Attempt %s, Reason: %s.', n, e)
                await asyncio.sleep(self.RECONNECT_INTERVAL)
            else:
                break
        self.conn.connection_lost_callback = self._on_connection_lost
        await self._setup_channels()
        self.log.info('Bus ready on amqp://%s', self.rmq_url.split('@')[-1])
        self._connecting = False

    def leave(self):
        self._leaving = True
        if self.conn:
            self.conn.close()
        if self._response_wait_task:
            self._response_wait_task.cancel()
        for c in self._consume_tasks:
            c.cancel()
        self._consume_tasks = []

    def reconnect(self):
        """
        Переподсоединение к брокеру
        """
        if self._connecting is True or self._leaving is True:
            return
        self._connecting = True
        self._exchanges = set()
        self._response_wait_task.cancel()
        for c in self._consume_tasks:
            c.cancel()
        self._consume_tasks = []
        task = asyncio.ensure_future(self.join(self.conn.ioloop.loop))
        task.add_done_callback(self.service.reconnect)

    async def set_pull_prefetch_count(self, count):
        await self.pull_channel.basic_qos(prefetch_count=count)
        self.pull_prefetch_count = count

    def _on_connection_lost(self, reason):
        """
        """
        self.log.warn("Connect lost, reason: %s", reason)
        self.log.warn("Scheduling reconnect")
        self.reconnect()

    async def _setup_channels(self):
        self.channel = await self.conn.channel()
        self.rpc_channel = await self.conn.channel()
        self.pull_channel = await self.conn.channel()
        # TODO: prefetch_count=1 не позволяет каналу принимать следующее сообщение,
        # пока он не подтвердит предыдущее, т.о. нельзя обрабатывать
        # несколько запросов параллельно.
        # Пока убираю лимит совсем, но может нужно опцией?
        if self.pull_prefetch_count:
            await self.pull_channel.basic_qos(prefetch_count=self.pull_prefetch_count)
        result = await self._declare_queue(self.channel, name='cbq', exclusive=True)
        self.callback_queue = result.method.queue
        self.log.debug('Callback queue is "%s"', self.callback_queue)
        self._response_wait_task = asyncio.ensure_future(self._wait_call_response())

    async def _wait_call_response(self):
        queue_object, _ = await self.channel.basic_consume(queue=self.callback_queue,
                                                           no_ack=True)
        self.log.debug('Consume on callback queue "%s"', self.callback_queue)
        while True:
            try:
                ch, method, props, body = await queue_object.get()
            except pika.exceptions.ChannelClosed as e:
                self.log.error('Channel Closed:, %s', e)
                self.log.info('Trying to reconnect...')
                self.reconnect()
                return
            try:
                msg = self.deserialize(body.decode())
                self._dispatch_response(props.correlation_id, msg)
            except Exception as e:
                self.log.error(" FIX ME!!! ERR : {}".format(e))  # TODO: обработка ошибок

    def _dispatch_response(self, correlation_id, msg):
        f = self._response_waiters.pop(correlation_id, None)
        if f is None:
            self.log.info('No waiters for response: {}#{}'.format(correlation_id, msg))
        else:
            f.cancelled() or f.set_result(msg)

    async def _init_exchange(self, name):
        if name not in self._exchanges:
            await self.channel.exchange_declare(exchange=name, type='fanout')
            self._exchanges.add(name)

    async def rpc_register(self, name, callable):
        """
        Регистрация RPC метода
        """
        result = await self._declare_queue(self.rpc_channel, name)
        self.log.info(
            'RPC  {:20} on {}'.format(name, result.method.queue))

        def wrap(cb):
            def _wrapper(msg):
                args, kw = msg
                return cb(*args, **kw)
            return _wrapper

        coro = self._consume(self.rpc_channel,
                             result.method.queue,
                             wrap(callable),
                             need_response=True)
        self._consume_tasks.append(asyncio.ensure_future(coro))

    async def rpc_call(self, name, *args, **kw):
        """
        Вызов RPC метода.
        """
        corr_id = str(uuid.uuid4())
        f = asyncio.Future()
        timeout = kw.pop('timeout', self.default_rpc_timeout)
        self._response_waiters[corr_id] = f

        await self._basic_publish(
            self.channel,
            exchange='',
            routing_key=self._ensure_bus_prefix(name),
            properties=self.make_message_props(reply_to=self.callback_queue,
                                               correlation_id=corr_id,
                                               expiration=str(timeout * 1000)),
            body=self.serialize((args, kw)))

        res = await asyncio.wait_for(f, timeout)
        self.maybe_raise_exception(res)
        return res

    async def push(self, job_name, *args, **kw):
        """
        Постановка задачи в очередь.

        Args:
            job_name(str): имя задачи
            *args: позиционные аргументы
            **kw: именные аргументы
            repeat_on_exception(int): число попыток выполнить задачу,
                если она падает с Exception. В случае Exception задача будет отправлена
                сервисом обратно в очередь с уменьшением атрибута repeat_on_exception на 1
        """
        props = self.make_message_props()
        repeat_on_exception = kw.pop('repeat_on_exception', None)
        if repeat_on_exception:
            assert isinstance(repeat_on_exception, int)
            props.headers['repeat_on_exception'] = repeat_on_exception
        await self._basic_publish(
            self.channel,
            exchange='',
            routing_key=self._ensure_bus_prefix(job_name),
            properties=props,
            body=self.serialize((args, kw)))
        return props.message_id

    async def pull(self, job_name, callable):
        """
        Регистрация PULL job (Задачи).

        Задача не отвечает ничего в брокер, задачи забираются из канала, для которого
        можно задать prefetch_count - максимальное число задач, которые сервис может взять
        из очереди без подтверждение == количество одновременно выпоняемых задач.

        Args:
            job_name(str): имя задачи
            callable: функция-обработчик
        """
        result = await self._declare_queue(self.pull_channel, job_name)
        self.log.info(
            'PULL {:20} on {}'.format(job_name, result.method.queue))

        def wrap(cb):
            def _wrapper(msg):
                args, kw = msg
                return cb(*args, **kw)
            return _wrapper

        coro = self._consume(self.pull_channel,
                             result.method.queue,
                             wrap(callable),
                             need_response=False)
        self._consume_tasks.append(asyncio.ensure_future(coro))

    async def subscribe(self, queue, callable):
        result = await self._declare_queue(self.channel, queue, exclusive=True)
        q = result.method.queue
        exchange_name = self._ensure_bus_prefix(queue)
        await self._init_exchange(exchange_name)
        self.channel.queue_bind(exchange=exchange_name, queue=q)
        task = asyncio.ensure_future(self._consume(self.channel, q, callable))
        self._consume_tasks.append(task)
        self.log.info('SUBSCRIBE on {} -> {}'.format(queue, callable))

    async def _consume(self, channel, queue, callable, need_response=False):
        queue_object, consumer_tag = await channel.basic_consume(queue=queue,
                                                                 no_ack=False)
        self.log.debug('Consume on queue "%s"', queue)
        while True:
            try:
                ch, method, props, body = await queue_object.get()
            except pika.exceptions.ChannelClosed as e:
                self.log.error('Channel Closed:, %s', e)
                self.log.info('Trying to reconnect...')
                self.reconnect()
                return
            asyncio.ensure_future(
                self._process_message(ch, method, props, body, callable, need_response))

    async def _process_message(self, ch, method, props, body, callable, need_response):
        """
        Корутина, обрабатывающая один RPC запрос.
        """
        exception = False
        headers = getattr(props, 'headers') or {}
        repeat_on_exception = headers.get('repeat_on_exception')
        try:
            msg = self.deserialize(body.decode('utf-8'))
            try:
                response = callable(msg)
                if asyncio.iscoroutine(response):
                    response = await response
            except Exception as e:
                if repeat_on_exception is not None:
                    self.log.exception('Job Exception: %s, from %s, attempts left: %s',
                                       self._call_to_str(method.routing_key, msg),
                                       props.app_id,
                                       repeat_on_exception)
                else:
                    self.log.exception('Process Message Exception %s', e)
                response = self.make_exception_response(e)
                exception = True

            if exception and repeat_on_exception:
                props.headers['repeat_on_exception'] -= 1
                await self._basic_publish(
                    self.channel,
                    exchange='',
                    routing_key=method.routing_key,
                    properties=props,
                    body=body)
            elif need_response:
                response = self.serialize(response)
                await self._basic_publish(
                    ch,
                    exchange='',
                    routing_key=props.reply_to,
                    properties=pika.BasicProperties(
                        correlation_id=props.correlation_id),
                    body=response)
        except Exception as e:
            self.log.exception("FIX ME!!! CERR : {}".format(e))  # TODO: обработка ошибок
        else:
            ch.basic_ack(delivery_tag=method.delivery_tag)

    async def _basic_publish(self, chan, *args, raise_exeption=False, **kw):
        """
        Обертка вокруг channel.basic_publish для перехвата ошибок и реконнекта
        """
        try:
            res = await chan.basic_publish(*args, **kw)
        except pika.exceptions.ChannelClosed as e:
            self.log.error('Channel Closed:, %s', e)
            self.log.info('Schedule reconnection.')
            self.reconnect()
            if raise_exeption is True:
                raise RuntimeError('Connection Lost') from e
        else:
            return res

    def unsubscribe(self, queue, callable):
        pass

    async def publish(self, queue, message):
        body = self.serialize(message)
        chan = self.channel
        exchange_name = self._ensure_bus_prefix(queue)
        await self._init_exchange(exchange_name)
        await self._basic_publish(chan,
                                  exchange=exchange_name,
                                  routing_key='',
                                  properties=self.make_message_props(),
                                  body=body)

    async def _declare_queue(self, channel, name=None, exclusive=False):
        """
        Создает очередь в RabbitMQ.
        Для обычной очереди добавляем префикс "{префикс шины}.{имя сервиса}.",
        для эксклюзивной - "{префикс шины}.{имя сервиса}.{id инстанса}.{random_uuid}
        """
        assert name or exclusive, "pass queue name or set exclusive=True"
        queue_name = self._ensure_bus_prefix(self.service_name)
        if name:
            queue_name = ".".join([queue_name, name])
        if exclusive is True:
            rand = str(uuid.uuid4())[:8]
            queue_name = '.'.join([queue_name, self.id, rand])
        self.log.debug('Declaring queue "%s"', queue_name)
        result = await channel.queue_declare(queue=queue_name, exclusive=exclusive)
        return result

    def make_message_props(self, **kw):
        """
        Создает Pika.BasicProperties c дефолтными аттрибутами message_id, headers,
            app_id, timestamp.
        """
        props = pika.BasicProperties()
        props.message_id = str(uuid.uuid4())
        props.headers = {}
        props.timestamp = int(time.time() * 10**6)
        props.app_id = '{}.{}'.format(self.service_name, self.id)
        for k, v in kw.items():
            setattr(props, k, v)
        return props

    def _call_to_str(self, key, msg):
        args, kw = msg
        name = key.lstrip(self._bus_prefix).lstrip('.')
        args_str = ','.join(
            list(map(str, args)) + [' {}={}'.format(k, v) for k, v in kw.items()])
        return "{}({})".format(name, args_str)
