# -*- coding:utf-8 -*-

import asyncio
from logging import getLogger

from .aio import RmqServiceBus


class BlockingRmqServiceBus(RmqServiceBus):
    """
    Сессия сервиса - подключение к шине
    Блокирующий вариант, для WSGI и консоли
    """

    log = getLogger('blocking_bus')

    def join(self, ioloop=None):
        ioloop = ioloop or asyncio.new_event_loop()
        asyncio.set_event_loop(ioloop)
        coro = super().join(ioloop)
        ioloop.run_until_complete(coro)

    def rpc_call(self, name, *args, **kw):
        coro = super().rpc_call(name, *args, **kw)
        return self.conn.ioloop.loop.run_until_complete(coro)

    def publish(self, queue, message):
        coro = super().publish(queue, message)
        return self.conn.ioloop.loop.run_until_complete(coro)

    def push(self, name, *args, **kw):
        coro = super().push(name, *args, **kw)
        return self.conn.ioloop.loop.run_until_complete(coro)
