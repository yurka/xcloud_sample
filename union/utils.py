# coding: utf-8

import asyncio
import funcy

@funcy.decorator
async def retry(call, tries, errors=Exception, timeout=0):
    """
    Порт http://funcy.readthedocs.org/en/stable/flow.html#retry для asyncio

    применять к корутин-функциям
    """
    if isinstance(errors, list):
        # because `except` does not catch exceptions from list
        errors = tuple(errors)

    for attempt in range(tries):
        try:
            return await call()
        except errors:
            # Reraise error on last attempt
            if attempt + 1 == tries:
                raise
            else:
                timeout_value = timeout(attempt) if callable(timeout) else timeout
                if timeout_value > 0:
                    await asyncio.sleep(timeout_value)
