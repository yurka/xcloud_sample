
import logging
import aiopg
import asyncio
from collections import defaultdict


class DbListener:
    log = logging.getLogger('db_listener')

    def __init__(self, dsn, poll_timeout=2, reconnect_timeout=5):
        self.dsn = dsn
        self._conn = None
        self._subscriptions = defaultdict(set)
        self._stopped = False
        self._futures = []
        self._reconnect_handle = None
        self._poll_timeout = poll_timeout
        self._reconnect_timeout = reconnect_timeout

    async def _create_connection(self, dsn):
        self._conn = await asyncio.wait_for(
            aiopg.connect(dsn, timeout=1),
            timeout=None)
        self.log.info('connected to: %s', self._conn.dsn)

    async def watch_connection(self):
        async with aiopg.connect(self.dsn) as conn:
            async with conn.cursor() as cur:
                while True:
                    try:
                        await cur.execute('SELECT 1')
                        await asyncio.sleep(self._poll_timeout)
                    except Exception as e:
                        self.log.error('error check connection alive: %s', e)
                        ioloop = asyncio.get_event_loop()
                        ioloop.call_soon(self._schedule_reconnect)
                        break

    async def _listen_and_watch(self):
        watch_fut = asyncio.ensure_future(self.watch_connection())
        for channel in self._subscriptions:
            await self._listen(channel)
        listen_fut = asyncio.ensure_future(self._check_notifies())
        self._futures = [watch_fut, listen_fut]

    def _schedule_reconnect(self):
        loop = asyncio.get_event_loop()

        def reconnect_cb():
            asyncio.ensure_future(self._reconnect())

        self._reconnect_handle = loop.call_later(self._reconnect_timeout, reconnect_cb)

    async def _reconnect(self):
        for f in self._futures:
            if not f.cancelled():
                f.cancel()
        await self._create_connection(self.dsn)
        if not self._conn:
            self._schedule_reconnect()
            return
        await self._listen_and_watch()

    async def start(self):
        await self._reconnect()

    async def _check_notifies(self):
        while not self._stopped:
            notify = await self._conn.notifies.get()
            self.log.debug('got notify pid=%s, channel=%s, payload=%s',
                           notify.pid,
                           notify.channel,
                           notify.payload)
            callbacks = tuple(self._subscriptions.get(notify.channel, ()))
            for f in callbacks:
                try:
                    asyncio.ensure_future(f(notify.payload))
                except Exception as err:
                    self.log.exception('error process callback: %s', err)

    def stop(self):
        self._stopped = True
        if self._reconnect_handle:
            self._reconnect_handle.cancel()
        self.close_connection()
        self._subscriptions.clear()

    async def subscribe(self, channel, callback):
        self._subscriptions[channel].add(callback)
        if self._conn and channel not in self._subscriptions:
            await self._listen(channel)

    async def _listen(self, channel):
        try:
            async with self._conn.cursor() as cur:
                await cur.execute('listen %s;' % channel)
                self.log.info('start listen %s', channel)
        except:
            self.log.exception('error subscribe to %s', channel)

    async def unsubscribe(self, channel, callback):
        if not self._conn or channel not in self._subscriptions:
            return
        self._subscriptions[channel].discard(callback)
        if not self._subscriptions[channel]:
            del self._subscriptions[channel]
            await self._unlisten(channel)

    async def _unlisten(self, channel):
        if self._conn:
            try:
                async with self._conn.cursor() as cur:
                    await cur.execute('unlisten %s;' % channel)
            except:
                self.log.exception('error unsubscribe from %s', channel)
