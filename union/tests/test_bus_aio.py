# coding: utf8

import asyncio

import pika
import asynctest.mock as mock
from . import TimedTestCase

from union.bus import aio


class RmqBusTest(TimedTestCase):
    # timeout = 100

    def setUp(self):
        self._mc_patch = mock.patch('union.bus.aio.make_connection')
        self.mock_mc = self._mc_patch.start()
        self.conn = self.make_conn()
        self.chan1, self.chan2, self.chan3, self.chan4 = self.conn.mchannels

    def tearDown(self):
        self._mc_patch.stop()

    def make_conn(self):
        conn = mock.CoroutineMock(name='conn')
        chan1 = mock.CoroutineMock(name='chan1')
        chan2 = mock.CoroutineMock(name='chan2')
        chan3 = mock.CoroutineMock(name='chan3')
        chan4 = mock.CoroutineMock(name='chan4')

        conn.mchannels = [
            chan1, chan2, chan3, chan4,
        ]
        conn.channel.side_effect = iter(conn.mchannels)
        return conn

    def make_bus(self, service_name='test_service', mock_conn=None):
        if mock_conn is not None:
            self.mock_mc.return_value = mock_conn
        else:
            self.mock_mc.return_value = self.conn
        service = mock.Mock()
        service.name = 'test_service'
        bus = aio.RmqServiceBus(service_name, 'amqp://a:b@c/d')
        bus.set_service(service)
        return bus

    def make_rmq_message(self, chan=None, method=None, props=None, body=None):
        chan = chan or mock.CoroutineMock()
        method = method or mock.Mock()
        props = props or mock.Mock()
        body = body or ''
        return chan, method, props, body

    def assert_props_equal(self, prop, **kw):
        """
        Сравнивает pika.BasicProperties
        """
        prop_2 = pika.BasicProperties(**kw)
        prop.timestamp = None
        assert prop.encode() == prop_2.encode()

    @mock.patch('uuid.uuid4')
    async def test_join(self, uid):
        uid.return_value = '01234567-abcd'

        q1 = asyncio.Queue()
        self.chan1.basic_consume.side_effect = [(q1, 'ctag1')]
        qm = mock.Mock()
        qm.method.queue = 'queue_1'
        self.chan1.queue_declare.side_effect = [qm]

        bus = self.make_bus()
        await bus.join()
        # нужно отдать управление планировщику,
        # чтобы таски отработали, до того как мы будем проверять вызов моков
        await asyncio.sleep(0)

        self.chan1.basic_consume.assert_called_with(queue=bus.callback_queue, no_ack=True)
        cb_queue_name = '_uc.test_service.cbq.abcd.01234567'
        self.chan1.queue_declare.assert_called_with(queue=cb_queue_name, exclusive=True),

        assert bus.callback_queue == 'queue_1'
        assert bus.id == 'abcd'

    @mock.patch('uuid.uuid4')
    async def test_reconnect(self, uid):
        uid.return_value = '01234567-abcd'

        q1 = asyncio.Queue()
        qm = mock.CoroutineMock(name='MyQueue')
        qm.get.side_effect = [pika.exceptions.ChannelClosed()]
        self.chan1.basic_consume.side_effect = [(q1, ''), (qm, '')]
        q2 = mock.Mock()
        q2.method.queue = 'queue_1'
        self.chan1.queue_declare.side_effect = [q2, q2]

        bus = self.make_bus()
        await bus.join()
        await asyncio.sleep(0)
        bus.join = mock.CoroutineMock()

        await bus.subscribe('foo', mock.Mock())
        await asyncio.sleep(0)
        self.assertEqual(bus.join.call_count, 1)

    @mock.patch('uuid.uuid4')
    async def test_subscribe(self, uid):
        uid.return_value = '01234567-abcd'

        conn = self.make_conn()
        bus = self.make_bus(mock_conn=conn)
        bus.conn = conn  # как будто уже сделали bus.join()
        bus.channel = conn.mchannels[0]
        bus.callback_queue = 'cbq'
        q1 = asyncio.Queue()
        bus.channel.basic_consume.side_effect = [(q1, 'ctag1')]

        qm = mock.Mock()
        qm.method.queue = 'foo'
        bus.channel.queue_declare.side_effect = [qm]

        cb = mock.Mock(name='cb')
        await bus.subscribe('foo', cb)
        await asyncio.sleep(0)
        body = bus.serialize("hello").encode()
        q1.put_nowait(self.make_rmq_message(body=body))
        await asyncio.sleep(0.01)

        qname = '_uc.test_service.foo.abcd.01234567'
        bus.channel.queue_declare.assert_called_with(queue=qname, exclusive=True)
        bus.channel.basic_consume.assert_called_with(queue='foo', no_ack=False)
        bus.channel.queue_bind.assert_called_with(exchange='_uc.foo', queue='foo')
        cb.assert_called_with("hello")

    @mock.patch('uuid.uuid4')
    async def test_rpc_call(self, uid):
        uid.return_value = URAND = '01234567-abcd'
        TIMEOUT = 1

        q1 = asyncio.Queue()
        self.chan1.basic_consume.side_effect = [(q1, 'ctag1')]
        qm = mock.Mock()
        qm.method.queue = 'queue_1'
        self.chan1.queue_declare.side_effect = [qm]
        self.chan1.basic_publish.return_value = None

        bus = self.make_bus()
        await bus.join()
        await asyncio.sleep(0)
        reply = self.make_rmq_message(
            props=mock.Mock(correlation_id=URAND),
            body=bus.serialize('w').encode()
        )
        q1.put_nowait(reply)
        res = await bus.rpc_call('foo.test', 1, 2, "hello", timeout=TIMEOUT)
        await asyncio.sleep(0)
        self.chan1.basic_publish.assert_called_with(
            body=bus.serialize(([1, 2, "hello"], {})),
            exchange='',
            routing_key='_uc.foo.test',
            properties=mock.ANY
            )
        call_props = self.chan1.basic_publish.call_args_list[0][1]['properties']
        self.assert_props_equal(call_props, correlation_id=URAND, reply_to='queue_1',
                                app_id='test_service.abcd', headers={},
                                message_id=URAND, expiration=str(TIMEOUT * 1000))
        assert res == 'w'

    @mock.patch('uuid.uuid4')
    async def test_publish(self, uid):
        uid.return_value = '111222333-xyz'
        qm = mock.Mock()
        qm.method.queue = 'publish_queue'
        self.chan1.queue_declare.side_effect = [qm]
        self.chan1.basic_publish.return_value = None

        bus = self.make_bus()
        await bus.join()
        await asyncio.sleep(0)
        await bus.publish('publish_queue', 'test_publish_message')
        await asyncio.sleep(0)
        self.chan1.basic_publish.assert_called_with(
            body=bus.serialize('test_publish_message'),
            exchange='_uc.publish_queue',
            routing_key='',
            properties=mock.ANY
        )

    @mock.patch('uuid.uuid4')
    async def test_rpc_register(self, uid):
        uid.return_value = URAND = '112233-abc'
        args = [('abc',), {}]
        q1 = asyncio.Queue()
        self.chan2.basic_consume.side_effect = [(q1, 'ctag1')]
        self.chan3.basic_publish.return_value = None
        qm = mock.Mock()
        qm.method.queue = 'queue_2'
        self.chan2.queue_declare.side_effect = [qm]

        bus = self.make_bus()
        await bus.join()
        bus.callback_queue = 'cbq'
        await asyncio.sleep(0)
        reply = self.make_rmq_message(
            props=mock.Mock(correlation_id=URAND),
            body=bus.serialize(args).encode()
        )
        q1.put_nowait(reply)
        cb_mock = mock.Mock()
        await bus.rpc_register('a_method', cb_mock)
        await asyncio.sleep(0.01)
        self.chan2.basic_consume.assert_called_with(queue='queue_2', no_ack=False)
        cb_mock.assert_called_with('abc')
