# coding: utf-8

import asyncio

import asynctest


class TimedTestCase(asynctest.TestCase):
    """
    Тест кейс с таймаутом на каждый тест.
    """

    timeout = 1

    def _run_test_method(self, method):
        result = method()
        if asyncio.iscoroutine(result):
            try:
                # добавляем таймаут на каждый тест
                task = asyncio.wait_for(result, self.timeout)
                self.loop.run_until_complete(task)
            except asyncio.TimeoutError:
                raise AssertionError('TimeoutError: %s seconds exceed' % self.timeout)
            return

        if (getattr(self.__class__, "__asynctest_ignore_loop__", False) or
                getattr(method, "__asynctest_ignore_loop__", False)):
            return

        if hasattr(self.loop, "__asynctest_ran") and not self.loop.__asynctest_ran:
            self.fail("Loop did not run during the test")
