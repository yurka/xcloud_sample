
import asyncio
from datetime import datetime

import crontab
from psycopg2.extras import DictCursor
from union.service import Service, pull


class UnsupportedTypeError(Exception):
    """
    Тип задачи не поддерживается.
    """


class SchedulerService(Service):
    """
    Сервис для запуска задач по расписанию.
    """

    INTERVAL = 5

    def __init__(self, name, bus, dbpool, interval=None):
        super().__init__(name, bus)
        self.dbpool = dbpool
        self._loop_task = None
        self.interval = interval or self.INTERVAL

    async def start(self, ioloop):
        await super().start(ioloop)
        self._loop_task = asyncio.ensure_future(self.loop())
        self.log.info('Scheduler started. Interval: %s.', self.interval)

    def stop(self):
        if self._loop_task:
            self._loop_task.cancel()
        super().stop()

    async def loop(self):
        while True:
            self.log.info('Fetching tasks...')
            try:
                tasks = await self.fetch_tasks()
                self.log.info('Got  %s tasks.', len(tasks))
                for task in tasks:
                    await self.maybe_run_task(task)
            except:
                self.log.exception("Sheduling Error:")
            await asyncio.sleep(self.interval)

    async def fetch_tasks(self):
        """
        Получить список активных задач из БД.
        """
        async with self.dbpool.acquire() as conn:
            async with conn.cursor(cursor_factory=DictCursor) as cur:
                await cur.execute("SELECT * from scheduler_task WHERE enabled=true;")
                ret = []
                async for row in cur:
                    ret.append(row)
                return ret

    async def get_task_by_id(self, task_id):
        """
        Получить задачу из БД по ID.
        """
        async with self.dbpool.acquire() as conn:
            async with conn.cursor(cursor_factory=DictCursor) as cur:
                await cur.execute("SELECT * from scheduler_task WHERE id=%s;", [task_id])
                return await cur.fetchone()

    async def maybe_run_task(self, task, force=False):
        """
        Проверить. нужно ли запустить задачу, и запустить, если нужно
        """
        entry = crontab.CronTab(task['cron'])
        now = datetime.now()
        next_now = entry.next(now)
        interval = next_now - entry.previous(now)
        last_run = task['last_run']
        last_run = last_run.replace(tzinfo=None)
        delta = (now - last_run).total_seconds()
        gap = 3 * self.INTERVAL
        if force is True or delta > interval or (next_now < gap and delta > gap):
            try:
                self.log.info('Run {type} {action} {args} {kwargs}'.format(**task))
                res = await self.do_run_task(task)
                self.log.info(
                    'Success {type} {action} {args} {kwargs} --> {result}'.format(
                        result=res, **task))
            except asyncio.TimeoutError:
                self.log.info(
                    'TimeoutError for {type} {action} {args} {kwargs}'.format(**task))
            except UnsupportedTypeError:
                self.log.error('Unsupported task type: "%s"', task['type'])
            except Exception:
                self.log.exception(
                    'Unexpected error for {type} {action} {args} {kwargs}'.format(**task))
            else:
                async with self.dbpool.acquire() as conn:
                    async with conn.cursor(cursor_factory=DictCursor) as cur:
                        await cur.execute('BEGIN;')
                        try:
                            await cur.execute(
                                "SELECT * FROM scheduler_task WHERE id=%s FOR UPDATE;",
                                (task['id'],))
                            await cur.execute(
                                "UPDATE scheduler_task SET last_run=%s WHERE id=%s;",
                                (now, task['id']))
                            await cur.execute('COMMIT;')
                        except:
                            await cur.execute('ROLLBACK;')

    async def do_run_task(self, task):
        """
        Запуск задачи - делаем rpc_call или publish
        """
        args = self.bus.deserialize(task['args'])
        kwargs = self.bus.deserialize(task['kwargs'])
        if task['type'] == 'RPC_CALL':
            res = await self.bus.rpc_call(task['action'], *args, **kwargs)
        elif task['type'] == 'PUBLISH':
            res = await self.bus.publish(task['action'], *args, **kwargs)
        elif task['type'] == 'PUSH':
            res = await self.bus.push(task['action'], *args, **kwargs)
        else:
            raise UnsupportedTypeError()
        return res

    @pull()
    async def run_task(self, task_id):
        """
        Ручной запуск задачи (из интерфейса шедулера в мониторе)
        """
        self.log.info('Force run task #{id}'.format(id=task_id))
        task = await self.get_task_by_id(task_id)
        if task:
            await self.maybe_run_task(task, force=True)
        else:
            self.log.warn('No task with ID #{id}'.format(**task))

