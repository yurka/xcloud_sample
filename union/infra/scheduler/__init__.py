
import aiopg
from union.bus.aio import RmqServiceBus
from union.infra.scheduler.service import SchedulerService


def service_factory(loop, config, args):
    dsn = config['MONITOR_DATABASE_URL']
    dbpool = loop.run_until_complete(aiopg.create_pool(dsn))
    bus = RmqServiceBus(rmq_url=config['BUS_BROKER_URL'])
    interval = config.get("SCHEDULER_INTERVAL")
    service = SchedulerService(args.name or 'scheduler', bus, dbpool, interval)
    return service
