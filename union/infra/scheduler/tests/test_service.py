# coding: utf8

import asyncio
from datetime import datetime

import asynctest.mock as mock
from union.tests import TimedTestCase
from union.bus.base import BaseServiceBus

from union.infra.scheduler.service import SchedulerService, UnsupportedTypeError


class MockContextManager:

    def __init__(self, val):
        self.val = val

    async def __aexit__(self, type, value, tb):
        return False

    async def __aenter__(self):
        return self.val


class MockIter:

    def __init__(self, val):
        self.val = iter(val)
        self._val = val

    async def __aiter__(self):
        return self

    async def __anext__(self):
        try:
            return self.val.__next__()
        except StopIteration:
            raise StopAsyncIteration()


class SchedulerTest(TimedTestCase):

    def mk_service(self):
        self.bus = mock.CoroutineMock(name='bus')
        self.bus.deserialize = lambda d: BaseServiceBus.deserialize(None, d)
        self.pool = mock.Mock(name='pool')
        self.conn = mock.Mock(name='conn')
        self.conn.cursor.return_value = MockContextManager(self.cur)

        self.cur.execute = mock.CoroutineMock()

        self.pool.acquire.return_value = MockContextManager(self.conn)
        s = SchedulerService('test_sched', self.bus, self.pool)
        return s

    def mk_task(self, **kw):
        task = {
            'id': 1,
            'type': 'RPC_CALL',
            'action': 'test',
            'args': '[]',
            'kwargs': '{}',
            'cron': '* * * * *',
            'last_run': datetime(2000, 1, 1)
        }
        task.update(kw)
        return task

    async def test_fetch_tasks(self):
        self.cur = MockIter(['1', 2])
        s = self.mk_service()
        tasks = await s.fetch_tasks()
        assert tasks == ['1', 2]

    @mock.patch('union.infra.scheduler.service.datetime')
    async def test_maybe_run_task(self, dt):
        self.cur = MockIter(['1', 2])
        s = self.mk_service()

        async def check(cron, last_run, now):
            dt.now.return_value = datetime(*now)
            t1 = self.mk_task(last_run=datetime(*last_run), cron=cron)
            s.do_run_task = mock.CoroutineMock()
            await s.maybe_run_task(t1)
            asyncio.sleep(0)
            return s.do_run_task.called

        assert await check('* * * * *', (2000, 1, 1), (2000, 1, 1)) is False
        assert await check('0 * * * *', (2000, 1, 1), (2000, 1, 1, 0, 59)) is False
        assert await check('0 * * * *', (2000, 1, 1), (2000, 1, 1, 1, 59)) is True
        assert await check('0,50 * * * *', (2000, 1, 1), (2000, 1, 1, 0, 1)) is False
        assert await check('0,50 * * * *', (2000, 1, 1), (2000, 1, 1, 0, 49)) is False
        assert await check('0,50 * * * *', (2000, 1, 1), (2000, 1, 1, 0, 51)) is True

    async def test_do_run_task(self):
        self.cur = MockIter(['1', 2])
        s = self.mk_service()

        t1 = self.mk_task()
        await s.do_run_task(t1)
        asyncio.sleep(0)
        self.bus.rpc_call.assert_called_with('test')
        self.bus.publish.assert_not_called()
        self.bus.rpc_call.reset_mock()
        self.bus.publish.reset_mock()

        t1 = self.mk_task(type='PUBLISH')
        await s.do_run_task(t1)
        asyncio.sleep(0)
        self.bus.rpc_call.assert_not_called()
        self.bus.publish.assert_called_with('test')
        self.bus.rpc_call.reset_mock()
        self.bus.publish.reset_mock()

        t1 = self.mk_task(action='foo.bar', args='[100, "Abc"]', kwargs='{"a":1}')
        await s.do_run_task(t1)
        asyncio.sleep(0)
        self.bus.rpc_call.assert_called_with('foo.bar', 100, 'Abc', a=1)
        self.bus.publish.assert_not_called()
        self.bus.rpc_call.reset_mock()
        self.bus.publish.reset_mock()

        t1 = self.mk_task(type="FOO_BAR")
        with self.assertRaises(UnsupportedTypeError):
            await s.do_run_task(t1)
            asyncio.sleep(0)
