from django.contrib import admin
from django.shortcuts import redirect, get_object_or_404
from django.conf.urls import url
from django.conf import settings
from django.contrib import messages

from union.bus.blocking import BlockingRmqServiceBus

from .models import Task


def enable_task(modeladmin, request, queryset):
    queryset.update(enabled=True)
enable_task.short_description = 'Активировать выбранные tasks'


def disable_task(modeladmin, request, queryset):
    queryset.update(enabled=False)
disable_task.short_description = 'Деактивировать выбранные tasks'


class TaskAdmin(admin.ModelAdmin):
    list_display = ['name', 'type', 'action',
                    'args', 'kwargs', 'enabled', 'cron',
                    'created', 'last_run', 'run_now']
    actions = [enable_task, disable_task]

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            url(r'^run_task/(?P<task_id>\d+)/$',
                self.admin_site.admin_view(self.run_task),
                name="run_task")
        ]
        return my_urls + urls

    def run_task(self, request, task_id):
        bus = BlockingRmqServiceBus('monitor', settings.BUS_BROKER_URL)
        bus.join()
        task = get_object_or_404(Task, pk=task_id)
        res = bus.push('scheduler.run_task', task_id=task_id)
        messages.add_message(request, messages.INFO,
                             'Task "{}" runs: {}.'.format(task.name, res))
        return redirect('admin:scheduler_task_changelist')

    def run_now(self, obj):
        return '<a href="run_task/{}/">run now</a>'.format(obj.pk)
    run_now.allow_tags = True



admin.site.register(Task, TaskAdmin)
