
from django.db import models
from django.core.exceptions import ValidationError

from union.bus.base import BaseServiceBus


class Task(models.Model):
    """
    Задача
    """
    PUBLISH = 'PUBLISH'
    RPC_CALL = 'RPC_CALL'
    PUSH = 'PUSH'
    TYPE_CHOICES = [
        (RPC_CALL, "RPC"),
        (PUBLISH, "PUBLISH"),
        (PUSH, "PUSH")
    ]

    name = models.CharField(max_length=50)
    type = models.CharField(max_length=50, choices=TYPE_CHOICES)
    action = models.CharField(max_length=50)
    args = models.CharField(
        max_length=200,
        default='[]',
        help_text="list в JOSN формате")
    kwargs = models.CharField(
        max_length=200,
        default='{}',
        help_text="dict в JOSN формате"
        )
    cron = models.CharField(
        max_length=50,
        help_text="урезанный синтаксис - только * или число "
                  "(числа через запятую): 5,15 3 * * *")
    enabled = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    last_run = models.DateTimeField(auto_now_add=True)

    class Meta:
        pass

    def clean(self):
        # Don't allow draft entries to have a pub_date.
        try:
            validate_crontab(self.cron)
        except ValidationError as e:
            raise ValidationError({'cron': e})

        try:
            args = BaseServiceBus.deserialize(None, self.args)
        except:
            raise ValidationError({'args': "Invalid args format, can't deserialize"})
        if not isinstance(args, list):
            raise ValidationError({'args': "Must be a list"})

        try:
            kwargs = BaseServiceBus.deserialize(None, self.kwargs)
        except:
            raise ValidationError({'kwargs': "Invalid args format, can't deserialize"})
        if not isinstance(kwargs, dict):
            raise ValidationError({'kwargs': "Must be a dict"})


def validate_crontab(tab_string):
    """
    Валидация crontab-строки.
    Пока не нашел общего способа для все вариантов,
    поэтому будем использовать урезанный синтаксис (только числа, через запятую).
    """
    slices = tab_string.split()
    if len(slices) != 5:
        raise ValidationError('Crontab slices len %s != 5' % len(slices))

    ranges = ((0, 59), (0, 23), (1, 31), (1, 12), (1, 7))

    def _check_digit(d, r):
        try:
            d = int(d)
        except (ValueError, TypeError):
            raise ValidationError('"%s" is not a digit' % d)
        if d < r[0] or d > r[1]:
            raise ValidationError("{} is out of range {}".format(d, r))

    for i, s in enumerate(slices):
        r = ranges[i]
        if s == '*':
            continue
        elif s.isdigit():
            _check_digit(s, r)
        else:
            digits = s.split(',')
            for d in digits:
                _check_digit(d, r)
