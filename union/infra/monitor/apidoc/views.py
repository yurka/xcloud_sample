import asyncio

from django.shortcuts import render
from django.conf import settings

from union.infra.monitor.bus_backend import RmqBusBackend
from union.bus.blocking import BlockingRmqServiceBus


def index_view(request):
    bus_backend = RmqBusBackend()
    services = []
    service_names = bus_backend.list_services()
    bus = BlockingRmqServiceBus('monitor', settings.BUS_BROKER_URL)
    bus.join()
    for service in service_names:
        try:
            doc = bus.rpc_call(service + '.api_doc', timeout=0.1)
        except asyncio.TimeoutError:
            doc = {'service_name': service, 'error': 'timeout'}
        except Exception:
            doc = {'service_name': service, 'error': 'other'}
        finally:
            services.append(doc)
    return render(request, "apidoc/index.html", {'services': services})
