import logging
import re

import requests

from django.conf import settings


logger = logging.getLogger('bus_backend')


class RmqBusBackend:

    def list_services(self):
        vhost = settings.BUS_BROKER_URL.split('/')[-1]
        queues = self.api_get('/queues/' + vhost)
        queues_names = [q['name'] for q in queues if q['name'].endswith('.api_doc')]

        def _extract_service_name(queue_name):
            return '.'.join(queue_name.split('.')[1:-1])

        servces = [_extract_service_name(q) for q in queues_names]
        return servces

    def api_get(self, path, **params):
        url = settings.BUS_BROKER_API_URL + path
        logger.info("Rabbit API query: %s, %s", re.sub('\/\/.*:.*@', '//', url), params)
        response = requests.get(url, params=params)
        logger.info("Rabbit API: %s", response.status_code)
        return response.json()
