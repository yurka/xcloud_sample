
import sys
import importlib
import asyncio
import functools
import signal
import argparse
import logging

import setproctitle

from union.service import config

DESCR = """
ulaunch - скрипт для запуска сервисов в Ucloud.
=====================================================]
Принимает два обязательных аргумента - path и environ,
path - Python путь к модулю, который содержит фабрику сервиса - функцию с
       именем "service_factory".
environ - имя среды для конфигурации.

def service_factory(loop, config, args):
    pass
    return service


Функция service_factory принимает 3 аргумента:
    loop - asyncio IOloop
    config - union.config.Config инстанс
    args - аргументы командной строки (parser.parse_args()).
           По умолчанию path, environ, loglevel, name.
           Cервис может добавить свои агрументы через атрибут фабрики __extra_args__:

            service_factory.__extra_args__ = [
                    (('--port',), dict(required=True, help='Web server port')),
                ]


Функция service_factory должна вернуть инстанс union.service.Service
"""


def main():

    parser = argparse.ArgumentParser(description=DESCR,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('path', help='Python path to module with service_factory')
    parser.add_argument('environ', default=None, nargs='?', help='Environment name')
    parser.add_argument("-l", "--loglevel", default='INFO', help="Log level")
    parser.add_argument('--name', help='Service name to register on bus')
    parser.add_argument('--config-prefix', default=None, help='prefix for FileConfig')

    if len(sys.argv) < 2 or sys.argv[1] == '-h':
        parser.print_help()
        exit()
    path = sys.argv[1]
    module = importlib.import_module(path)
    service_factory = getattr(module, 'service_factory')

    extra_args = getattr(service_factory, '__extra_args__', [])
    for arg, kw in extra_args:
        parser.add_argument(*arg, **kw)

    args = parser.parse_args()
    cfg = config.FileConfig(args.environ, args.config_prefix)
    logging.basicConfig(
        format='%(asctime)-15s %(levelname)-8s %(name)s: %(message)s',
        level=args.loglevel
    )

    loop = asyncio.get_event_loop()
    service = service_factory(loop, cfg, args)

    setproctitle.setproctitle("{}:{}".format(cfg.environ, service.name))

    loop = asyncio.get_event_loop()

    def ask_exit(signame):
        logging.info("Got signal %s, exiting...", signame)
        service.stop()
        loop.stop()

    for signame in ('SIGINT', 'SIGTERM'):
        loop.add_signal_handler(getattr(signal, signame),
                                functools.partial(ask_exit, signame))

    try:
        asyncio.ensure_future(service.start(loop))
        loop.run_forever()
    finally:
        loop.close()


if __name__ == '__main__':
    main()
