import time
import subprocess

import psycopg2
import psycopg2.extras
import dj_database_url


def mk_db_conn(union_config, dbname):
    return psycopg2.connect(
        dbname=dbname,
        user=union_config["TEST_DB_USER"],
        host=union_config["TEST_DB_HOST"],
        port=union_config["TEST_DB_PORT"])


DELETE_FOLDER_PROC = """
do $$import os
import shutil
try:
    shutil.rmtree('/var/comagic_storage/')
    os.mkdir('/var/comagic_storage/')
except:
    pass
$$ language plpythonu
"""

def create_databases(config, union_config):
    """
    Создает базы данных для теста.

    Имя базы берется из конфига, также нужно указать путь к исходникам БД
    """

    if not config.option.reuse_db:

        _create_database_from_sources(
            union_config,
            src_path=union_config["BDD_COMAGIC_DB_SRC"],
            db_name='comagic_test_0'
            )

        _create_database_from_sources(
            union_config,
            src_path=union_config["BDD_COMAGIC_DB_SRC"]+ '/proxy',
            db_name='comagic_test'
            )

        _create_database_from_sources(
            union_config,
            src_path=union_config["BDD_BILLING_DB_SRC"],
            db_name='billing_test'
            )

        # _create_database_from_sources(
        #     union_config,
        #     src_path=union_config["BDD_INFOPIN_DB_SRC"],
        #     db_name='infopin_test'
        #     )
        db_port = int(union_config["TEST_DB_PORT"])

        conn = mk_db_conn(union_config, 'comagic_test')
        with conn.cursor() as cur:
            cur.execute("INSERT INTO node (id) VALUES (0)")
            cur.execute(DELETE_FOLDER_PROC)
            cur.execute("GRANT USAGE ON FOREIGN SERVER comagic TO webuser")
            cur.execute("GRANT USAGE ON FOREIGN SERVER comagic TO billing")
            cur.execute(
                "ALTER SERVER comagic OPTIONS ( set node_0 'dbname=comagic_test_0 port=%s')", [db_port])
        conn.commit()
        conn.close()

        conn = mk_db_conn(union_config, 'comagic_test_0')
        with conn.cursor() as cur:
            cur.execute(
                "ALTER SERVER billing OPTIONS ( set node_0 'dbname=billing_test port=%s')", [db_port])
        conn.commit()
        conn.close()

        conn = mk_db_conn(union_config, 'billing_test')
        with conn.cursor() as cur:
            cur.execute(
                "ALTER SERVER comagic OPTIONS ( set node_0 'dbname=comagic_test port=%s')", [db_port])
            cur.execute(
                "ALTER SERVER infopin OPTIONS ( set node_0 'dbname=infopin_test port=%s')", [db_port])
        conn.commit()
        conn.close()

        register_user(union_config, 'test_user')

    else:
        print("Reusing databases")


def register_user(union_config, name):
    conn = mk_db_conn(union_config, 'comagic_test_0')
    with conn.cursor() as cur:
        cur.execute("insert into dial_rules_group (id, name) values (1, '800')")
    conn.commit()
    conn.close()

    conn = mk_db_conn(union_config, 'billing_test')
    with conn.cursor() as cur:
        cur.execute(("select * from add_registration("
                     "%s, '71111111111', 'a@mail.me', null, 1, null, null)"), [name])
        row = cur.fetchone()
    conn.commit()
    
    with conn.cursor() as cur:
        cur.execute("select permission.set_auth_info(null, 0, 0, 'system', true, '0.0.0.0')")
        sql = "select * from accept_registration(%s)"
        cur.execute(sql, [row[0]])
    conn.commit()
    conn.close()


def _create_database_from_sources(union_config, src_path, db_name):
    print("Creating database", db_name)
    result = subprocess.run(["pg_import", src_path],
                            stdout=subprocess.PIPE)
    assert result.returncode == 0
    sql = result.stdout

    conn = mk_db_conn(union_config, 'postgres')
    conn.autocommit = True

    with conn.cursor() as cur:
        cur.execute("SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid();")
        cur.execute("DROP DATABASE IF EXISTS %s" % db_name)
        cur.execute("CREATE DATABASE %s" % db_name)
    conn.close()

    args = ["psql", "-U", union_config["TEST_DB_USER"],
            "-h", union_config["TEST_DB_HOST"], "-p", str(union_config["TEST_DB_PORT"])]
    result = subprocess.run(args + [db_name], input=sql,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    assert result.returncode == 0


def populate_db_table(union_config, current_scenario, dbname, table_name, data):
    current_scenario.__tables_to_truncate__.add((dbname, table_name))
    keys = list(data[0].keys())
    values_str = ','.join(keys)
    params_str = ','.join(['%({})s'.format(key) for key in keys])
    query = "INSERT INTO {} ({}) VALUES ({})".format(table_name, values_str, params_str)

    if dbname == 'comagic':
        dbname += '_test_0'
    else:
        dbname += '_test'
    conn = mk_db_conn(union_config, dbname)
    with conn.cursor() as cur:
        cur.executemany(query, data)
    conn.commit()
    conn.close()


def truncate_db_tables(union_config, tables):
    conn = {'comagic': mk_db_conn(union_config, 'comagic_test_0'),
            'billing': mk_db_conn(union_config, 'billing_test'),
            'infopin_test': mk_db_conn(union_config, 'billing_test')}
    for db, t in tables:
        with conn[db].cursor() as cur:
            cur.execute("TRUNCATE TABLE {} RESTART IDENTITY CASCADE".format(t))
    for c in conn.values():
        c.commit()
        c.close()
    conn = mk_db_conn(union_config, 'comagic_test_0')
    # with conn.cursor() as cur:
    #     cur.execute("TRUNCATE TABLE dial_rules_group RESTART IDENTITY CASCADE")
    conn.commit()
    conn.close()


def execute_sql(union_config, dbname, query):
    if dbname == 'comagic':
        dbname += '_test_0'
    else:
        dbname += '_test'
    conn = psycopg2.connect(
            dbname=dbname,
            user=union_config["TEST_DB_USER"],
            host=union_config["TEST_DB_HOST"],
            port=union_config["TEST_DB_PORT"],)
    with conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:
        cur.execute(query)
        res = cur.fetchall()
    conn.close()
    return res

_union_services = []


def start_union_services(union_config):
    services = [
        ('comagic_asi.sync_worker',),
        ('comagic_asi.web', '--port', '9800')
    ]
    global _union_services

    ulaunch_path = union_config['VENV'] + '/bin/ulaunch'
    stdout_file = open('/tmp/union_test_stdout', 'w')
    for service in services:
        args = [ulaunch_path, service[0], union_config.environ] + list(service[1:])
        proc = subprocess.Popen(args, stdout=stdout_file, stderr=subprocess.STDOUT)
        _union_services.append(proc)
        print('Spawned {} with pid {}'.format(service, proc.pid))
    time.sleep(0.2)  # дадим сервисам подняться


def stop_union_services(union_config):
    global _union_services
    for proc in _union_services:
        proc.terminate()
        proc.wait()
