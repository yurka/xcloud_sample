Feature: Site
    A site to test.

Scenario: Test site sync
    When I regiser user foo
    And comagic table analytics.site is
        | id | app_id |
        | 40 | 12     |
        | 50 | 12     |
    And comagic table ppc.analytics is
        | id | app_id | access_token | ext_login | engine        |
        | 1  | 12     | QWERTY       | foo       | yandex.direct |
        | 2  | 12     | ZXC          | bar       | google.adwords|
    And comagic table ppc.site_analytics is
        | id | analytics_id | site_id | engine         | 
        | 1  | 1            | 40      | yandex.direct  |
        | 2  | 2            | 50      | google.adwords |
    And http request localhost:9899/api/?q=12 responds with 200 and
        {"status": "OK",
            "data":[
                {"app_id": 22, "site_id": 40, "domain": "buzz"}]}

    Then rpc_call("comagic_asi.sync_worker.get_analytics", 12, 40, "yandex.direct") returns subset
        {"access_token": "QWERTY", "app_id": 12}
    And comagic table analytics.site like
        | id | app_id |
        | 40 | 12     |
        | 50 | 12     |