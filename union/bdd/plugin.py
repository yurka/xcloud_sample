import urllib.request
import urllib.parse
import csv
import time
import io
import re
import json
import threading
import copy
import decimal
import datetime
from unittest import mock
from wsgiref.util import is_hop_by_hop

import requests
import jinja2
import pytest
from pytest_bdd import given, when, then, parsers

from union.service import config
from union.bdd import utils
from union.bus.blocking import BlockingRmqServiceBus


terminal = None
union_config = config.FileConfig()

# setup jinja2 templates env:
jinja2_env = jinja2.Environment()


def _now(fmt="%Y-%m-%d %H:%M:%S", **delta_kwargs):
    now = datetime.datetime.now()
    if delta_kwargs:
        now = now + datetime.timedelta(**delta_kwargs)
    return now.strftime(fmt)

_template_gobals = {"now": _now}
jinja2_env.globals.update(_template_gobals)


def assert_dict_subset_of(superset, subset):
    superset_copy = copy.deepcopy(superset)
    superset_copy.update(subset)
    assert superset == superset_copy 


@pytest.fixture
def uconfig():
    return union_config


@pytest.fixture
def bus():
    bus = BlockingRmqServiceBus("pytest", union_config["BUS_BROKER_URL"])
    bus.join()
    return bus


@pytest.hookimpl(trylast=True)
def pytest_configure(config):
    global terminal
    terminal = config.pluginmanager.getplugin("terminalreporter")
    utils.create_databases(config, union_config)
    # register_user('test_user')
    utils.start_union_services(union_config)


@pytest.hookimpl(trylast=True)
def pytest_unconfigure(config):
    utils.stop_union_services(union_config)


def pytest_addoption(parser):
    """Add pytest-bdd options."""
    group = parser.getgroup("union_bdd", "Ucloud BDD support")
    group.addoption(
        "--reuse-db",
        action="store_true",
        dest="reuse_db",
        default=False,
        help="Reuse test databases - not delete database after tests",
    )


_current_scenario = None
@pytest.fixture
def current_scenario():
    return _current_scenario


@pytest.hookimpl(trylast=True)
def pytest_bdd_before_scenario(request, feature, scenario):
    scenario.__tables_to_truncate__ = set()
    global _current_scenario
    _current_scenario = scenario


@pytest.hookimpl(trylast=True)
def pytest_bdd_after_scenario(request, feature, scenario):
    utils.truncate_db_tables(union_config, _current_scenario.__tables_to_truncate__)
    report = request.node.__scenario_report__

    failed = any([r.failed for r in report.step_reports])

    if failed:
        def write(content):
            terminal.write(content, red=True)
    else:
        def write(content):
            terminal.write(content, green=True)

    def writeln(content):
        terminal.write(content + '\n')

    terminal.write('~' * 80 + '\n', red=failed, green=not failed)
    terminal.write("Feature: ", red=failed, green=not failed, bold=True)
    terminal.write(report.scenario.feature.name + '\n', red=failed, green=not failed)
    terminal.write("Scenario: ", red=failed, green=not failed, bold=True)
    terminal.write(report.scenario.name + '\n', red=failed, green=not failed, bold=True)

    def write_step(step_report):
        markup = dict(red=step_report.failed, green=not step_report.failed)
        terminal.write('\t' + step_report.step.keyword, bold=True, **markup)
        terminal.write('\t' + step.step.name.replace('\n', '\n\t\t\t') + '\n', **markup)

    for step in report.step_reports:
        write_step(step)


from werkzeug.wrappers import Request


def _make_any(data, any_marker='ANY'):
    for k, v in data.items():
        if v == any_marker:
            data[k] = mock.ANY
        elif isinstance(v, dict):
            data[k] = _make_any(v, any_marker) 
        elif isinstance(v, list):
            data[k] = list(map(lambda _v: mock.ANY if _v == any_marker else _v, v))
    return data


class HTTPServer:

    def __init__(self, port):
        self.port = port
        self._responses = []

    def add_response(self, url, query, payload, status, body,
                     target=None, find=None, replace=None):
        query = _make_any(query, ['ANY'])
        if isinstance(payload, dict):
            payload = _make_any(payload)
        self._responses.append((url, query, payload, status, body, target, find, replace))

    def application(self, environ, start_response):

        request = Request(environ)
        url = request.path

        payload = request.data.decode()
        query = dict(request.args)
        try:
            payload = json.loads(payload)
        except:
            pass
        # print(">>>> HTTP REQUEST", url, payload)
        for r in self._responses[:]:
            if r[0] == url and r[1] == query and r[2] == payload:
                self._responses.remove(r)
                # print(">>>> HTTP REQUEST MATCH", url, payload)
                if r[5] is None:
                    status = '200 OK'
                    headers = [('Content-type', 'text/plain; charset=utf-8')] # HTTP Headers
                    body = r[4]
                else:
                    status, headers, body = self._proxy_request(r[5], r[1])
                    if r[6] and r[7]:
                        body = body.replace(r[6], r[7])
                break
        else:
            print(">>>> HTTP REQUEST MISS", url, query, payload)
            # import ipdb;ipdb.set_trace();
            raise AssertionError("No HTTP response for %s %s" % (url, payload))
        start_response(status, headers)
        return [body.encode()]

    def start(self):
        from wsgiref.simple_server import make_server
        self.httpd = make_server('', self.port, self.application)
        self.server_thread = threading.Thread(
            target=lambda: self.httpd.serve_forever())
        self.server_thread.start()

    def stop(self):
        self.httpd.shutdown()
        self.httpd.server_close()
        self.server_thread.join()

    def _proxy_request(self, url, query):
        resp = requests.get(url, query)
        status = "{} {}".format(resp.status_code, resp.reason)
        headers = [(k, v) for k, v in resp.headers.items() if not is_hop_by_hop(k)]
        body = resp.content
        print("PROXY:", url, query, status, 'XX')
        return status, headers, body.decode()


@pytest.yield_fixture
def http_server(port=9899):
    server = HTTPServer(port)
    server.start()
    print("\nStarting live HTTP server on port", port)
    yield server
    server.stop()


def _coerse(str_val):
    """
    приведение строк из описания таблиц к типам питона
    """
    if re.match("^'[^\']*'$", str_val): # строки в кавычках
        return str_val.strip("'")
    if re.match("^\[.*\]$", str_val): # массивы
        return json.loads(str_val.replace("'", '"'))
    spec_types = {'false': False, 'true': True, 'null': None}
    if str_val in spec_types: # True, False, Null
        return spec_types[str_val]
    for _type in (int, float, str): # остальное
        try:
            return _type(str_val)
        except:
            continue


def parse_table_data(data):
    data = jinja2_env.from_string(data).render()
    reader = csv.DictReader(io.StringIO(data), delimiter='|', skipinitialspace=True)
    parsed_data = []
    for row in reader:
        d = {}
        for k, v in row.items():
            key, value = k.strip(), v.strip()
            if key:
                d[key] = _coerse(value)
        parsed_data.append(d)
    return parsed_data


@when(parsers.parse('{dbname} table {table_name} is\n{data}'))
def setup_table(dbname, table_name, data, current_scenario):
    utils.populate_db_table(union_config, current_scenario,
                            dbname, table_name, parse_table_data(data))
    return data

@when(parsers.parse('{dbname} query {query}'))
def run_sql_query(dbname, query):
    utils.execute_sql(union_config, dbname, query)

# @when(parsers.parse('I register user {name}'))
# def register_user(name):
#     # current_scenario.__tables_to_truncate__.add(('billing', 'table_name'))
#     conn = utils.mk_db_conn(union_config, 'comagic_test_0')
#     with conn.cursor() as cur:
#         cur.execute("insert into dial_rules_group (id, name) values (1, '800')")
#     conn.commit()
#     conn.close()


#     conn = utils.mk_db_conn(union_config, 'billing_test')
#     with conn.cursor() as cur:
#         cur.execute("select * from add_registration(%s, '71111111111', 'a@mail.me', null, 1, null, null)", [name])
#         row = cur.fetchone()
#     conn.commit()

    
#     with conn.cursor() as cur:
#         cur.execute("select permission.set_auth_info(null, 0, 0, 'system', true, '0.0.0.0')")
#         sql = "select * from accept_registration(%s)"
#         cur.execute(sql, [row[0]])
#     conn.commit()
#     conn.close()



@when(parsers.parse('http request {url} proxy to\n{target} replacing\n{find}with\n{replace}'))
def setup_http_service(url, target, find, replace, http_server):
    url = urllib.parse.urlparse(url)
    query_params = urllib.parse.parse_qs(url.query)
    http_server.add_response(url.path, query_params, '', None, None,
                             target=target.strip(), find=find.strip(),
                             replace=replace.strip())


@when(parsers.parse('http request {url} proxy \n{target}'))
def setup_http_service(url, target, http_server):
    url = urllib.parse.urlparse(url)
    query_params = urllib.parse.parse_qs(url.query)
    http_server.add_response(url.path, query_params, '', None, None, target=target)


@when(parsers.parse('http request {url} responds with {status} and\n{response}'))
def setup_http_service(url, status, response, http_server):
    url = urllib.parse.urlparse(url)
    query_params = urllib.parse.parse_qs(url.query)
    http_server.add_response(url.path, query_params, mock.ANY, status, response)


@when(parsers.parse('http request {url} with json payload\n{json_payload}\nresponds with {status} and\n{response}'))
def setup_http_service_json(url, json_payload, status, response, http_server):
    json_payload = json.loads(json_payload)
    url = urllib.parse.urlparse(url)
    query_params = urllib.parse.parse_qs(url.query)
    http_server.add_response(url.path, query_params, json_payload, status, response)


@when(parsers.parse('i do request {url}'))
def do_request(url):
    requests.get(url)


@when(parsers.parse('push({call})'))
def do_push(call, bus):
    endpoint, arg, kw = _parse_bus_call(call)
    bus.push(endpoint, *arg, **kw)


@when(parsers.parse('i do POST {url} with json payload\n{json_payload}'))
def setup_http_service_json(url, json_payload):
    requests.post(url, data=json_payload)


@when(parsers.parse('wait {sec}s'))
def wait(sec):
    time.sleep(float(sec))


def _coerce_db_types(dataset):
    res = []
    for row in dataset:
        new_row = {}
        for k, v in row.items():
            if isinstance(v, decimal.Decimal):
                new_row[k] = float(v)
            elif isinstance(v, datetime.date):
                new_row[k] = v.strftime("%Y-%m-%d")
            elif isinstance(v, datetime.datetime):
                new_row[k] = v.strftime("%Y-%m-%d %H:%M:%S")
            else:
                new_row[k] = v
        res.append(new_row)
    return res                


@then(parsers.parse('{dbname} table {table_name} is\n{data}'))
def check_table(dbname, table_name, data):
    expected_result = parse_table_data(data)
    query = "SELECT * FROM " + table_name
    result = utils.execute_sql(union_config, dbname, query)
    result = _coerce_db_types(result)
    assert result == expected_result


@then(parsers.parse('{dbname} table {table_name} like\n{data}'))
def check_table(dbname, table_name, data):
    expected_result = parse_table_data(data)
    query = "SELECT * FROM " + table_name
    result = utils.execute_sql(union_config, dbname, query)
    result = _coerce_db_types(result)
    assert len(result) == len(expected_result)
    for i, row in enumerate(result):
        assert_dict_subset_of(row, expected_result[i])


@then(parsers.parse('{dbname} query {query} result is\n{data}'))
def check_sql_query_result(dbname, query, data):
    expected_result = json.loads(data)
    result = utils.execute_sql(union_config, dbname, query)
    assert result == expected_result


def _parse_bus_call(call):
    call = [c.strip() for c in call.split(',')]
    endpoint, params = json.loads(call[0]), call[1:]
    arg = [p for p in params if '=' not in p]
    kw = [p.replace('=', ':') for p in params if '=' in p]
    arg = json.loads('[{}]'.format(','.join(arg)))
    kw = json.loads('{{{}}}'.format(','.join(kw)))
    return endpoint, arg, kw


@then(parsers.parse('rpc_call({call}) returns subset\n{expected_result}'))
def check_rpc_call_subset(call, expected_result, bus):
    expected_result = json.loads(expected_result)
    endpoint, arg, kw = _parse_bus_call(call)
    actual_result = bus.rpc_call(endpoint, *arg, **kw)
    assert_dict_subset_of(actual_result, expected_result)


@then(parsers.parse('rpc_call({call}) returns\n{expected_result}'))
def check_rpc_call(call, expected_result, bus):
    expected_result = json.loads(expected_result)
    endpoint, arg, kw = _parse_bus_call(call)
    actual_result = bus.rpc_call(endpoint, *arg, **kw)
    assert actual_result == expected_result


@then(parsers.parse('rpc_call({call}) raises Exception'))
def check_rpc_call(call, bus):
    endpoint, arg, kw = _parse_bus_call(call)
    with pytest.raises(Exception):
        bus.rpc_call(endpoint, *arg, **kw)


@then(parsers.parse('http {url} responds with {status} and\n{body}'))
def http_responds(url, status, body):
    import urllib.request
    r = urllib.request.urlopen(url)
    data = r.read()
    assert data == body


@then(parsers.parse('http POST {url} with json payload\n{json_payload}\nresponds with {status:d} and\n{response}'))
def setup_http_service_json(url, json_payload, status, response, http_server):
    r = requests.post(url, data=json_payload)
    assert r.status_code == status
    expected_response = json.loads(response)
    for k, v in expected_response.items():
        if v == 'ANY':
            expected_response[k] = mock.ANY
    assert expected_response == r.json()


@then(parsers.parse('truncate {dbname} {table_name}'))
def truncate_table(dbname, table_name):
    utils.truncate_db_tables(union_config, [(dbname, table_name)])