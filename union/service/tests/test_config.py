
import builtins
from unittest import mock

import pytest

from union.service.config import FileConfig, ConfigError


@pytest.fixture
def open(monkeypatch):
    f = mock.Mock(name='cf')
    m = mock.Mock(return_value=f)
    m.file = f
    monkeypatch.setattr(builtins, 'open', m)
    return m


def test_file_config_ok(open):
    open.file.read.return_value = '{"a":"b", "c": [1,2,3]}'
    c = FileConfig('test1')
    assert c['a'] == 'b'
    assert c['c'] == [1, 2, 3]
    with pytest.raises(KeyError):
        c['b']
    open.assert_called_once_with('/etc/union/test1.conf', 'r')


def test_file_config_ok_prefix(open):
    open.file.read.return_value = '{"a":"b", "c": [1,2,3]}'
    c = FileConfig('test1', '/foo')
    assert c['a'] == 'b'
    assert c['c'] == [1, 2, 3]
    with pytest.raises(KeyError):
        c['b']
    open.assert_called_once_with('/foo/test1.conf', 'r')


def test_file_config_err_json(open):
    open.file.read.return_value = '["a", "b", "c", [1,2,3]]'
    with pytest.raises(ConfigError) as err_info:
        FileConfig('test1')
    assert "got <class 'list'>" in err_info.value.args[0]


def test_file_config_err_open(open):
    open.side_effect = OSError('foo bar')
    with pytest.raises(ConfigError) as err_info:
        FileConfig('test1')
    assert ("Can not open config file /etc/union/test1.conf: foo bar" ==
            err_info.value.args[0])
