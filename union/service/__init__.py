# -*- coding:utf-8 -*-
"""
Каркас для сервисов.
"""

import asyncio
import logging
import inspect


def _get_sig(func):
    return str(inspect.signature(func)).replace('(self, ', '(').replace('(self)', '()')


def register(name=None):
    def _wrapper(func):
            _name = name or func.__name__
            func.__bus_register_params__ = {
                'name': _name,
                'sig': _get_sig(func),
                'method': func.__name__,
                'doc': func.__doc__,
                'type': 'rpc'
                }
            return func
    return _wrapper


def subscribe(channal=None):
    def _wrapper(func):
            _name = channal or func.__name__
            func.__bus_register_params__ = {
                'name': _name,
                'sig': _get_sig(func),
                'method': func.__name__,
                'doc': func.__doc__,
                'type': 'sub'
                }
            return func
    return _wrapper


def pull(name=None):
    def _wrapper(func):
            _name = name or func.__name__
            func.__bus_register_params__ = {
                'name': _name,
                'sig': _get_sig(func),
                'method': func.__name__,
                'doc': func.__doc__,
                'type': 'pull'
                }
            return func
    return _wrapper


class ServiceMeta(type):
    """
    Метакласс Сервиса - для отложенной регистрации обработчиков на шине.
    """

    def __new__(cls, name, bases, dct):
        ncl = type.__new__(cls, name, bases, dct)
        ncl.__bus_registrations__ = {}
        for name in dir(ncl):
            method = getattr(ncl, name, None)
            if callable(method) and hasattr(method, '__bus_register_params__'):
                params = method.__bus_register_params__
                ncl.__bus_registrations__[params['name']] = params
        return ncl


class Service(metaclass=ServiceMeta):
    """
    Базовый класс для сервиса

    Реализует запуск-остановку сервиса и регистрацию обработчиков на шине.
    """

    def __init__(self, name, bus):
        self.name = name
        self.bus = bus
        self.bus.set_service(self)
        self.log = logging.getLogger(name)
        self.running = False
        self.ioloop = None

    async def _apply_registrations(self):
        for name, params in self.__bus_registrations__.items():
            if params['type'] == 'rpc':
                await self.bus.rpc_register(name, getattr(self, params['method']))
            elif params['type'] == 'sub':
                await self.bus.subscribe(name, getattr(self, params['method']))
            elif params['type'] == 'pull':
                await self.bus.pull(name, getattr(self, params['method']))

    def reconnect(self, *args):
        self.log.warn('Reconnecting!')
        asyncio.ensure_future(self._apply_registrations())

    @register()
    def api_doc(self):
        """
        Метод, отдающий документацию всех зарегистрированных в шине методов
        """
        return {
            'service_name': self.name,
            'doc': self.__doc__,
            'registrations': self.__bus_registrations__
        }

    async def start(self, ioloop):
        self.log.info('Starting Service "%s".', self.name)
        self.ioloop = ioloop
        await self.bus.join(ioloop)
        await self._apply_registrations()
        self.running = True
        self.log.info('Service "%s" up and running.', self.name)

    def stop(self):
        self.log.info('Stopping Service "%s".', self.name)
        self.bus.leave()
