
import os
import json


class ConfigError(Exception):
    """
    Ошибка в конфиге
    """


class Config:
    """
    Реестр настроек сервисов.

    Dict-like объект с доступом на чтение.
    """

    def __init__(self, environ=None):
        if not environ:
            try:
                environ = os.environ['UCLOUD_ENV']
            except KeyError:
                raise ConfigError(
                    "Config environ undefined. (You may use UCLOUD_ENV environ variable)") 
        self.environ = environ
        self._config = {}
        self._load_config()

    def __getitem__(self, key):
        """
        """
        return self._config[key]

    def get(self, key):
        """
        """
        return self._config.get(key)

    def _load_config(self):
        raise NotImplementedError()


class FileConfig(Config):
    """
    Реестр настроек, читающий настройки из файла {self.prefix}/{sefl.environ}.conf

    Attributes:
        environ(str): имя "среды" - dev, prod, local, vasya и т.д. для указания
            конкретного файла. Eсли не задано - используется переменная среды UCLOUD_ENV.
        prefix(str): префикс для файла с настройками. Eсли не задано - используется
            переменная среды UCLOUD_ENV_PREFIX, a по умолчанию /etc/union
    """
    prefix = "/etc/union"

    def __init__(self, environ=None, prefix=None):
        prefix = prefix or os.environ.get('UCLOUD_ENV_PREFIX')
        if prefix:
            self.prefix = prefix
        super().__init__(environ)

    def _load_config(self):
        filename = "{}/{}.conf".format(self.prefix, self.environ)
        try:
            config_file = open(filename, 'r')
        except Exception as e:
            raise ConfigError(
                "Can not open config file {}: {}".format(filename, e)) from e
        try:
            data = json.loads(config_file.read())
        except Exception as e:
            raise ConfigError(
                "Can not load config file {}: {}".format(filename, e)) from e

        if not isinstance(data, dict):
            raise ConfigError("Config must be a Dict, got {}".format(type(data)))

        self._config.update(data)
